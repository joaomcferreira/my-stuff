Credits to: https://stackoverflow.com/questions/10310299/proper-way-to-sudo-over-ssh

These perl and ruby scripts contain simple examples of how one could automate
remote execution over ssh with sudo authentication being required on the remote host.

The sudo password is piped to sudo's stdin, through the ssh tunnel. The password is
never written or visible anywhere, except locally on the password file.


                                      ssh tunnel
                            =============================
 "password for sudo | " ------->                     -------> " | sudo -S .... "
                            =============================

enjoy
jmf, 20220212
