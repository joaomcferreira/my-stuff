use strict;
use warnings;

###############################################################################
print "\n";
print "========== example a =======\n";
my $cmd_a = 'cat remote_sudo_password.txt | ssh -tt jmf@10.0.2.15 "sudo whoami"';
system($cmd_a);
print "============================\n";
sleep 1;
# ========== example a ==========
# jjj
# [sudo] password for jmf:
# root
# Connection to 10.0.2.15 closed.
# ===============================
###############################################################################


###############################################################################
print "\n";
print "========== example b =======\n";
my $cmd_b = 'cat remote_sudo_password.txt | ssh jmf@10.0.2.15 "cat | sudo -S --prompt=' . "''" . ' -- whoami"';
system($cmd_b);
print "============================\n";
sleep 1;
# ========== example b ==========
# root
# ===============================
###############################################################################


###############################################################################
print "\n";
print "========== example c =======\n";
my $cmd_c = 'cat remote_sudo_password.txt | ssh jmf@10.0.2.15 "cat | sudo -S --prompt=' . "''" .' -- ';
my $cmd_c_a = $cmd_c . 'whoami"';# root is expected here !
my $cmd_c_r = $cmd_c . 'ls -la /root/"'; #this would be impossible whitout root privileges
my $cmd_c_b = $cmd_c . 'head -n 3 /etc/sudoers"'; #this would be impossible whitout enough privileges
my $cmd_c_c = $cmd_c . 'systemctl stop cron"';    #this would also be impossible whitout enough privileges
my $cmd_c_d = $cmd_c . 'systemctl start cron"';   #same here
my $cmd_c_e = $cmd_c . 'systemctl status cron | grep Active"';
print "===== whoami ===============\n"; print $cmd_c_a . "\n"; system($cmd_c_a); sleep 1;
print "===== /root/ ===============\n"; print $cmd_c_r . "\n"; system($cmd_c_r); sleep 1;
print "===== /etc/sudoers =========\n"; print $cmd_c_b . "\n"; system($cmd_c_b); sleep 1;
print "===== cron stop ============\n"; print $cmd_c_c . "\n"; system($cmd_c_c); sleep 1;
print "===== cron status ==========\n"; print $cmd_c_e . "\n"; system($cmd_c_e); sleep 1;
print "===== cron start ===========\n"; print $cmd_c_d . "\n"; system($cmd_c_d); sleep 1;
print "===== cron status ==========\n"; print $cmd_c_e . "\n"; system($cmd_c_e); sleep 1;
print "============================\n";
# ========== example c =======
# ===== whoami ===============
# cat remote_sudo_password.txt | ssh jmf@10.0.2.15 "cat | sudo --prompt='' -S -- whoami"
# root
# ===== /root/ ===============
# cat remote_sudo_password.txt | ssh jmf@10.0.2.15 "cat | sudo --prompt='' -S -- ls -la /root/"
# total 64
# drwx------  8 root root  4096 Mar  5 19:36 .
# drwxr-xr-x 20 root root  4096 Nov  3 18:20 ..
# -rw-------  1 root root 10931 Mar  5 19:36 .bash_history
# -rw-r--r--  1 root root   570 Jan 31  2010 .bashrc
# drwx------  4 root root  4096 Aug  5  2019 .cache
# drwxr-xr-x  7 root root  4096 Aug  5  2019 .config
# drwx------  4 root root  4096 Sep 27 13:39 .gnupg
# drwxr-xr-x  2 root root  4096 Jan 28  2019 lixo
# drwxr-xr-x  3 root root  4096 Jan 14  2019 .local
# -rw-r--r--  1 root root    41 Nov 13  2020 more_apts.txt
# -rw-r--r--  1 root root   148 Aug 17  2015 .profile
# drwx------  2 root root  4096 Jan 14  2019 .ssh
# -rw-r-----  1 root root     4 Mar  5 19:17 .vboxclient-display-svga.pid
# -rw-------  1 root root   110 Mar  5 19:35 .Xauthority
# ===== /etc/sudoers =========
# cat remote_sudo_password.txt | ssh jmf@10.0.2.15 "cat | sudo --prompt='' -S -- head -n 3 /etc/sudoers"
# #
# # This file MUST be edited with the 'visudo' command as root.
# #
# ===== cron stop ============
# cat remote_sudo_password.txt | ssh jmf@10.0.2.15 "cat | sudo --prompt='' -S -- systemctl stop cron"
# ===== cron status ==========
# cat remote_sudo_password.txt | ssh jmf@10.0.2.15 "cat | sudo --prompt='' -S -- systemctl status cron | grep Active"
#    Active: inactive (dead) since Sat 2022-03-05 19:45:45 WET; 1s ago
# ===== cron start ===========
# cat remote_sudo_password.txt | ssh jmf@10.0.2.15 "cat | sudo --prompt='' -S -- systemctl start cron"
# ===== cron status ==========
# cat remote_sudo_password.txt | ssh jmf@10.0.2.15 "cat | sudo --prompt='' -S -- systemctl status cron | grep Active"
#    Active: active (running) since Sat 2022-03-05 19:45:48 WET; 1s ago
# ============================
