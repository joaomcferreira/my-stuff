puts '========== example b =======';
remote = "cat | sudo --prompt='' -S -- whoami"
cmd_b = 'cat remote_sudo_password.txt | ssh jmf@192.168.1.76 "' + remote  + '"';
puts cmd_b
puts "============================";
system cmd_b;
puts "============================";
# ========== example b ==========
#cat remote_sudo_password.txt | ssh jmf@192.168.1.76 "cat | sudo --prompt='' -S -- whoami"
#============================
# root
# ===============================
puts '========== example c =======';
remote = "cat | sudo --prompt='' -S -- ls -la /root/"
cmd_b = 'cat remote_sudo_password.txt | ssh jmf@192.168.1.76 "' + remote  + '"';
puts cmd_b
puts "============================";
system cmd_b;
puts "============================";
#========== example c =======
#cat remote_sudo_password.txt | ssh jmf@192.168.1.76 "cat | sudo --prompt='' -S -- ls -la /root/"
#============================
#total 64
#drwx------  8 root root  4096 Feb 14 10:40 .
#drwxr-xr-x 20 root root  4096 Nov  3 18:20 ..
#-rw-------  1 root root 10826 Feb  6 17:57 .bash_history
#-rw-r--r--  1 root root   570 Jan 31  2010 .bashrc
#drwx------  4 root root  4096 Aug  5  2019 .cache
#-rw-r--r--  1 root root   148 Aug 17  2015 .profile
#============================
