#equire_relative 'lib/base_data'
require_relative 'lib/app/ctx'

ctx = Ctx.new #this is Inversion of Control && Dependency Injection

## early clone because of jobs ################################################
ctx.generic.banner 'will clone the sources, start a fresh DB, setup the sources and run the tests'
sleep 1
ctx.specific.perform_clone_and_bundle(ctx)

###############################################################################
ctx.generic.banner 'will start a fresh DB, clone the sources, setup the sources and run the tests'
sleep 1

## 1db ########################################################################
if ARGV.include?('noErasePgData') then
  ctx.generic.banner 'skipping erase of existing Pg data (as requested)'
else
  ctx.docker.clear_all_docker_volumes(ctx)
end
sleep 1

if ARGV.include?('noStartDB') then
  ctx.generic.banner 'skipping start of Pg service (as requested)'
else
  ctx.docker.fork_start_docker_db(ctx)
end
sleep 1

ctx.tests.perform_tests_run(ctx, 'check_tcp_ports', 1)

if ARGV.include?('noRestoreDump') || ARGV.include?('noErasePgData') then
  ctx.generic.banner 'skipping restore of dump file (as requested)'
else
  ctx.specific.perform_drop_db_and_restore_dump(ctx)
end
sleep 1

ctx.specific.perform_wait_for_pg(ctx)

## 2clone #####################################################################
ctx.specific.perform_clone_and_bundle(ctx)
#tx.dirtyd.do_your_thing(ctx)

## 3setup #####################################################################
ctx.bdata.perform_add_base_data(ctx)

## 4test ######################################################################
ctx.tests.perform_tests_run(ctx)

## That's all, folks !!! ######################################################
