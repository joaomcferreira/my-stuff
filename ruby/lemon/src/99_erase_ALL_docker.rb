require_relative 'lib/app/ctx'
ctx = Ctx.new

ctx.generic.banner 'erasing all existing docker containers, images, volumes, networks, ... anything!'
ctx.docker.erase_ALL_docker(ctx)
