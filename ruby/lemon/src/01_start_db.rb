require_relative 'lib/app/ctx'
#
#see also:
# 11_start_jobs.rb
# 12_start_infra.rb
#
ctx = Ctx.new
ctx.generic.banner 'Starting leemonidas database and stuff...'
ctx.docker.normal_start_docker_db(ctx);
