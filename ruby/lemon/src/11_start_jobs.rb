require_relative 'lib/app/ctx'
#
#see also:
# 01_start_db.rb
# 12_start_infra.rb
#
ctx = Ctx.new
ctx.jobs.start_jobs(ctx)
