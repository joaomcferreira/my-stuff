require_relative 'lib/app/ctx'
#
#see also:
# 01_start_db.rb
# 11_start_jobs.rb
#
ctx = Ctx.new
ctx.generic.banner 'Starting infrastructure'
ctx.docker.fork_start_docker_db(ctx);
