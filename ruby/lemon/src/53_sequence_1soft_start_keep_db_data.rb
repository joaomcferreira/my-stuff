require_relative 'lib/app/ctx'

ctx = Ctx.new

###############################################################################
ctx.generic.banner 'will do a soft start...'
sleep 1

## early clone #####################################################################
ctx.specific.perform_clone_and_bundle(ctx)
#tx.dirtyd.do_your_thing(ctx)

## 1db (keep existing data) ###################################################
ctx.docker.stop_all_docker_containers(ctx)
ctx.docker.fork_start_docker_db(ctx)
sleep 1
ctx.tests.perform_tests_run(ctx, 'check_tcp_ports', 1)
ctx.specific.perform_wait_for_pg(ctx)

## 2setup #####################################################################
ctx.specific.perform_config(ctx)
ctx.specific.perform_db_migrate(ctx)

## 3test ######################################################################
ctx.tests.perform_tests_run_by_list_n_times(ctx, 'sine_qua_non', 1)
