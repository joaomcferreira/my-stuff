require_relative 'lib/app/ctx'

ctx = Ctx.new #this is Inversion of Control && Dependency Injection
ctx.dirtyd.do_your_thing(ctx)
