require_relative 'lib/app/ctx'

ctx = Ctx.new

## 1 - clone #####################################
ctx.specific.perform_clone_and_bundle(ctx)

ctx.tests.perform_tests_run(ctx, 'check_tcp_ports', 1)

## 2 - setup ######################################
ctx.specific.perform_config(ctx)
ctx.specific.perform_db_migrate(ctx)

## 3 - test ######################################
ctx.tests.perform_tests_run(ctx)
