require_relative '../util'
require_relative '../jobs'
require_relative '../psqlx'
require_relative '../tests'
require_relative '../config'
require_relative '../docker'
require_relative '../generik'
require_relative '../specifics'
require_relative '../base_data'
require_relative '../dirty_deeds'

class Ctx
    #lazy-loaded or instantiated from outside
    @obj_util     = nil
    @obj_jobs     = nil
    @obj_psql     = nil
    @obj_tests    = nil
    @obj_bdata    = nil
    @obj_config   = nil
    @obj_docker   = nil
    @obj_dirtyd   = nil
    @obj_generic  = nil
    @obj_specific = nil
    def util     (x = nil) @obj_util     = x ||     Util.new unless @obj_util     ; return @obj_util     ; end
    def jobs     (x = nil) @obj_jobs     = x ||     Jobs.new unless @obj_jobs     ; return @obj_jobs     ; end
    def psql     (x = nil) @obj_psql     = x ||    PsqlX.new unless @obj_psql     ; return @obj_psql     ; end
    def tests    (x = nil) @obj_tests    = x ||    Tests.new unless @obj_tests    ; return @obj_tests    ; end
    def bdata    (x = nil) @obj_bdata    = x || BaseData.new unless @obj_bdata    ; return @obj_bdata    ; end
    def config   (x = nil) @obj_config   = x ||   Config.new unless @obj_config   ; return @obj_config   ; end
    def docker   (x = nil) @obj_docker   = x ||   Docker.new unless @obj_docker   ; return @obj_docker   ; end
    def dirtyd   (x = nil) @obj_dirtyd   = x ||   DirtyD.new unless @obj_dirtyd   ; return @obj_dirtyd   ; end
    def generic  (x = nil) @obj_generic  = x ||  Generik.new unless @obj_generic  ; return @obj_generic  ; end
    def specific (x = nil) @obj_specific = x || Specifik.new unless @obj_specific ; return @obj_specific ; end
end
