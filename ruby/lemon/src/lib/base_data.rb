class BaseData

  ###############################################################################
  def perform_add_base_data(ctx)
    #TODO_jmf: implement add_base_data
    ctx.generic.banner 'add_base_data is not ready... yet... skip forced'
    ctx.generic.banner 'add_base_data is not ready... yet... skip forced'
    ctx.generic.banner 'add_base_data is not ready... yet... skip forced'
    return if ctx.config.should_skip_add_base_data(ctx)
    raise 'add_base_data is not ready... yet... skip forced'
  end

  ###############################################################################
  def load_base_data_pre(ctx)
    ctx.generic.banner 'load_base_data_pre'
    ctx.generic.banner 'load_base_data_pre done'
  end

  ###############################################################################
  def load_base_data_leem(ctx, ruby_fixture_file, arg1, arg2, arg3)

    ctx.generic.banner 'load_base_data_fixtures'

    require ruby_fixture_file

    psql_base_command = ctx.psql.psql_cmd_new_base(ctx)

    SpecCommonFixtures::GenericBaseData.new(psql_base_command, arg1, arg2, arg3)

    ctx.generic.banner 'load_base_data_fixtures done'
  end

  ###############################################################################
  def load_base_data_post(ctx, arg1, arg2, arg3, arg4)
    ctx.generic.banner 'load_base_data_post'

    tasks = []

    tasks.push({ type:  'shell', task: 'uname -a' })
    tasks.push({ type:  'shell', task: 'cat /etc/os-release' })

    ctx.util.execute_tasks(ctx, tasks, 'load_base_data_post')
    ctx.generic.banner 'load_base_data_post done'
  end

  ###############################################################################
end
