class Generik

  #############################################
  def banner(what)
    puts '====> ' + what.to_s
    sleep 0.01
  end

  #############################################
  def repeat_ntimes_unless_command_exits_0(ntimes, command, title = nil)
    puts 'ntimes is less than 1. nothing to do, really.' if ntimes < 1

    durations = []
    i = 1
    while i <= ntimes  do
      banner_text = "loop #{i} / #{ntimes}"
      banner banner_text
      t_a = Time.new().strftime('%s%L').to_i
      exit_1_unless_command_exits_0(command, title, banner_text)
      t_b = Time.new().strftime('%s%L').to_i
      took = t_b - t_a
      durations.push(took)
      banner "done #{i} / #{ntimes} (took: #{took} ms)"
      i +=1
    end
    puts durations
  end

  ###############################################################################
  def exit_1_verbose(what)
    puts what + ' can not proceed; exit 1.'
    exit 1
  end

  #############################################
  def exit_1_unless_command_exits_0(command, title = nil, exit_1_message = '...')
    ok = run_shell_command(command, title)
    exit_1_verbose 'FATAL: command did not exit 0 (at ' + exit_1_message + '); command was |' + command + '|;' unless ok
  end

  #############################################
  def run_shell_command(cmd, title = nil)
    banner(title) if title
    puts '----> ' + cmd
    ok = system cmd
    puts '-----'
    sleep 0.01
    return ok
  end

  #############################################
  def exit_1_unless_pre_requisites_ok()
    needed = [
      'ruby -v',
      'docker -v',
      'bundle -v',
      'redis-cli -v',
      'git --version',
      'psql --version',
      'docker-compose -v',
      'patch -v | grep GNU',
      'tar --version | grep tar',
      'ls -l $HOME/.ssh/id_rsa.pub',
      'docker image ls && docker container ls',
      'echo "seems like you got everything installed, cool!"',
    ]

    needed.each { |x| exit_1_unless_command_exits_0(x) }
  end

  #############################################
  def exit_1_unless_file_exists(path, prefix, suffix)
    unless File.exist?(path)
      puts "#{prefix}#{path} not found; #{suffix}can not proceed."
      exit 1
    end
    puts "#{path} found; will proceed;"
    puts ''
  end

  #############################################
  def wait_for_file_exists(path, reason)
    max = 100
    count = 0

    puts "wait_for_file_exists (#{path}, #{reason})"

    while count < max
      count = count + 1
      break if File.exist?(path)
      puts "file not found yet (#{count}/#{max}) (#{path} #{reason}); waiting..."
      sleep 1
      sleep 9 if count > 9
    end

    x =  File.exist?(path) ? '' : 'NOT '
    puts "file #{x}found #{path}"
  end

  #############################################
  def exit_1_unless_dir_exists(path, prefix, suffix)
    unless Dir.exist?(path)
      puts "#{prefix}#{path} does not exist. #{suffix}can not proceed."
      exit 1
    end
  end

  #############################################
  def dir_exists_else_command(path, suffix, command)
    if Dir.exist?(path)
      puts "directory #{path} exists; #{suffix}"
      return
    end
    run_shell_command(command)
  end
  #############################################

end
