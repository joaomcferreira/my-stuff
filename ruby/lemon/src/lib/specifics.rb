require 'open3'
require 'etc'

class Specifik

  ###############################################################################
  def perform_clone_and_bundle(ctx)
    ctx.generic.banner 'cloning...'

    repo_url  = ctx.config.get_repo_url()
    clone_dir = ctx.config.get_clone_dir()
    branch    = ctx.config.cfg_get_branch()
    rrr_dir   = ctx.config.get_rrr_dir()

    ctx.generic.banner "clone; directory: #{clone_dir}; branch: #{branch};"

    ctx.generic.dir_exists_else_command(clone_dir, 'not cloning;',
     'git clone -b ' + branch + ' ' + repo_url + ' ' + clone_dir)

    ctx.generic.exit_1_unless_dir_exists clone_dir, 'FATAL ', 'directory still does not exist after cloning. weeeiiiiiird! '

    c_out, c_err, c_st = Open3.capture3("cd #{clone_dir} && git rev-parse --abbrev-ref HEAD")
    branch_in_dir = c_out.chomp
    ctx.generic.exit_1_verbose "ERROR: git branch in #{clone_dir} is not #{branch} (it is #{branch_in_dir});" unless (branch == branch_in_dir);

    ctx.generic.run_shell_command "cd #{clone_dir} && git pull", 'git pull' if ctx.config.cfg_get_pull_before_bundle()

    ctx.dirtyd.do_your_thing(ctx)

    bundle_dir = ctx.config._get_cfg_bundle_dir()
    n_procs = Etc.nprocessors
    n_procs = n_procs - 1 if n_procs > 1
    bundle_cmd  = 'bundle -v && bundle'
    bundle_cmd += " --jobs #{n_procs}"
    bundle_cmd += " --path #{bundle_dir}" if bundle_dir
    #undle_cmd = "bundle config set --local path #{bundle_dir} && bundle" if bundle_dir (deu asneira)
    #undle_cmd = "bundle install --path #{bundle_dir}" if bundle_dir # (este funciona)

    override_bundle_cmd = ctx.config._get_cfg_general_by_key('override_bundle_cmd')
    bundle_cmd = override_bundle_cmd if override_bundle_cmd

    dont_bundle = ctx.config._get_cfg_general_by_key('dont_bundle')

    ctx.generic.banner 'bundle ruby dir'
    if dont_bundle
      ctx.generic.banner 'will _NOT_ bundle ruby dir (as requested by dont_bundle cfg option)'
    else
      ctx.generic.run_shell_command "cd #{rrr_dir} && #{bundle_cmd}"
    end
    ctx.generic.exit_1_unless_command_exits_0 "cd #{rrr_dir} && bundle check", nil

    #configuration yml files
    #ctx.generic.run_shell_command "cp -v config/*.yml #{clone_dir}/config/", 'configuration files'

    if ARGV.include?('stopAfterBundle') then
      puts 'stopping after bundle (as requested)'
      exit 1
    end

  end

  ###############################################################################
  def perform_config(ctx)
    ctx.generic.banner 'perform_config is not ready... yet... skip forced'
    ctx.generic.banner 'perform_config is not ready... yet... skip forced'
    ctx.generic.banner 'perform_config is not ready... yet... skip forced'
  end

  ###############################################################################
  def perform_db_migrate(ctx)
    ctx.generic.banner 'perform_db_migrate is not ready... yet... skip forced'
    ctx.generic.banner 'perform_db_migrate is not ready... yet... skip forced'
    ctx.generic.banner 'perform_db_migrate is not ready... yet... skip forced'
  end

  ###############################################################################
  def perform_wait_for_pg(ctx)
    pg_host = ctx.config.cfg_get_db_host()
    pg_port = ctx.config.cfg_get_db_port()
    ctx.generic.banner "check connection to postgres (#{pg_host}:#{pg_port})"

    x = ctx.psql.wait_for_pg(pg_host, pg_port, 999, 0.5, 4)

    if x > 0
      puts 'FATAL: could not connect to postgresql. can not proceed.'
      exit 1
    end
    puts 'connection to postgresql ok. will proceed.'
  end

  ###############################################################################
  def perform_drop_db_and_restore_dump(ctx)

    dmp_file = ctx.config.cfg_get_dump_file()
    pg_host  = ctx.config.cfg_get_db_host()
    pg_port  = ctx.config.cfg_get_db_port()
    hp_flags = ctx.config.get_db_hp_string()
    toto_flg = ctx.config.get_db_ud_string()
    to_pass  = ctx.config.get_db_password()
    pass_to  = 'PGPASSWORD=' + to_pass
    poto_flg = '-U postgres leemonidas'
    popo_flg = '-U postgres postgres'
    pg_pass  = 'postgres'
    pass_pg  = 'PGPASSWORD=' + pg_pass

    ctx.generic.banner 'Hello, good day to you :)'
    perform_wait_for_pg(ctx)

    ctx.generic.banner 'drop database leemonidas'
    ctx.generic.run_shell_command "#{pass_pg} dropdb #{hp_flags} #{poto_flg}"


    ctx.generic.banner 'createuser leemonidas'
    ctx.generic.run_shell_command "#{pass_pg} createuser #{hp_flags} #{poto_flg}"
    ctx.generic.run_shell_command "#{pass_pg} psql #{hp_flags} #{popo_flg} -c " +
      '"ALTER ROLE leemonidas WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD ' + "'leemonidas'" + '"'


    ctx.generic.banner 'list users'
    ctx.psql.exec_psql_qry(ctx, hp_flags, popo_flg, pg_pass, '\du')


    ctx.generic.banner 'createdb leemonidas'
    ctx.generic.run_shell_command "#{pass_pg} createdb #{hp_flags} #{poto_flg}"


    ctx.generic.banner 'list databases'
    ctx.psql.exec_psql_qry(ctx, hp_flags, popo_flg, pg_pass, '\l')


    #ctx.generic.banner 'create 100 tablespaces'
    #command = ctx.psql.load_sql_file_command(ctx, 'base_schema/create_100_tablespaces.sql')
    #command += ' >> /tmp/leemonidas_junk.txt'
    #ctx.generic.run_shell_command command
    ctx.psql.exec_psql_qry(ctx, hp_flags, popo_flg, pg_pass, '\db+')
    #tx.psql.exec_psql_qry(ctx, hp_flags, popo_flg, pg_pass, '\db+ tablespace_*9')


    ctx.generic.banner 'search for dump file'
    ctx.generic.exit_1_unless_file_exists dmp_file, 'ERROR: ', ''

    ti = Time.now.to_i
    if dmp_file.end_with?('.sql.tar.gz')
      sql_file = '/tmp/leemonidas_extracted.sql'
      cmd = "tar -zxOvf #{dmp_file} >#{sql_file}"
      ctx.generic.exit_1_unless_command_exits_0 cmd, "extracting #{dmp_file}"
      dmp_file = sql_file
    end

    banner = '?'
    full_cmd = 'we_no_need_no_stinking_bages'
    if dmp_file.end_with?('.sql')
      banner = "loading #{dmp_file} with psql"
      full_cmd = ctx.psql.load_sql_file_command(ctx, dmp_file)
      full_cmd += ' >>/tmp/leemonidas_junk.txt' unless ctx.config.cfg_get_verbose()
    else
      banner = "restoring #{dmp_file} with pg_restore"
      cmd = 'pg_restore'
      cmd += ' -v' if ctx.config.cfg_get_verbose()
      full_cmd = "#{pass_to} #{cmd} #{hp_flags} -U  leemonidas -d  leemonidas #{dmp_file}"
    end
    ctx.generic.exit_1_unless_command_exits_0 full_cmd, banner

    tf = Time.now.to_i
    duration = tf - ti
    ctx.generic.banner "restore of dump took #{duration} seconds"


    if ARGV.include?('stopAfterRestore') then
      puts 'stopping after restore as requested'
      exit 1
    end


    ctx.generic.banner 'list databases'
    ctx.psql.exec_psql_qry_new(ctx, '\l')


    ctx.generic.banner 'set config variables'
    ctx.psql.exec_many_psql_qrys(ctx,
      [
        "ALTER DATABASE  leemonidas SET  leemonidas.abc TO '1'",
      ]
    )


    exit 1 if ctx.config.user_wants_casper_mode(ctx)

  end

  ###############################################################################
end
