require 'yaml'

class Tests

  #############################################################################
  def load_test_list(data, name)
    x = []
    puts 'loading test list: ' + name.to_s
    if data[name]
      data[name].each do |ta|
        if data[ta]
          load_test_list(data, ta).each do |tb|
            x.push(tb)
          end
        else
          x.push(ta)
        end
      end
    else
      puts "no such test list: #{name}. can not proceed. exit 1."
      exit 1
    end
    return x
  end

  #############################################################################
  def perform_tests_run_by_list_n_times(ctx, test_list_name, n_times)
    src_dir = ctx.config.get_clone_dir()
    rrr_dir = ctx.config.get_rrr_dir()
    test_list_file = "#{src_dir}/spec/sql/unit/test_lists.yml"
    test_list_file = 'test_lists.yml' if File.exist?('test_lists.yml')
    ts  = load_test_list(YAML.load_file(test_list_file), test_list_name)
    ts  = ts.shuffle if ctx.config._get_cfg_general_by_key('shuffle_test_list')
    ssv = ts.join(' ')
    opt = ctx.config.cfg_get_rspec_opts()
    tst_dir = ctx.config.get_tst_dir()

    cmd = "cd #{rrr_dir} && TEST_CONFIG_DIR=#{tst_dir}/config bundle exec rspec #{opt} #{ssv}"
    puts 'test command is: |' + cmd + '|'

    exit 1 if ctx.config.should_skip_tests(ctx)

    ctx.generic.repeat_ntimes_unless_command_exits_0 n_times, cmd, "running tests #{test_list_name}"
  end

  #############################################################################
  def perform_tests_run(ctx, list = nil, n_times = nil)
    src_dir = ctx.config.get_clone_dir()
    tst_dir = ctx.config.get_tst_dir()

   #unless ctx.config.should_skip_pre_tests(ctx) || list
   #  perform_tests_run_by_list_n_times(ctx, 'check_tcp_ports', 1)
   #  perform_tests_run_by_list_n_times(ctx, 'sine_qua_non', 1)
   #end

    name = list
    name = ctx.config.cfg_get_test_list_name() unless name
    n = n_times
    n = ctx.config.cfg_get_run_tests_n_times() unless n
    n = 3 unless n
    perform_tests_run_by_list_n_times(ctx, name, n)
  end

  #############################################################################

end
