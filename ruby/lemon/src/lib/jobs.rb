class Jobs

  #############################################################################
  def start_jobs(ctx)
    ctx.specific.perform_wait_for_pg(ctx) #jobs will not start unless services are up&running; must wait;

    rrr_dir = ctx.config.get_rrr_dir()
    ctx.generic.wait_for_file_exists("#{rrr_dir}/Gemfile", 'needed to start stuff in ruby')

    ctx.generic.banner 'starting jobs (hopefully...)'
    sleep 1
    ctx.generic.banner 'starting jobs (hopefully...)'
    sleep 1
  end

  #############################################################################
end
