class Util

  def execute_tasks(ctx, tasks, title = nil)
    title = 'executing tasks' unless title
    ctx.generic.banner title

    tasks.each_with_index { |t|
      t_type = t[:type]
      t_task = t[:task]

      #########################################################################
      unless t_type
        ctx.generic.banner 'ERROR t_type not defined'
        next
      end

      unless t_task
        ctx.generic.banner 'ERROR t_task not defined'
        next
      end

      #########################################################################
      if t_type == 'psql'
        ctx.psql.exec_psql_qry_new ctx, t_task

      elsif t_type == 'banner'
        ctx.generic.banner t_task

      elsif t_type == 'shell'
        ctx.generic.run_shell_command t_task

      elsif t_type == 'sql_file'
        ctx.psql.load_sql_file ctx, t_task

      else
        ctx.generic.banner 'ERROR task type not supported: ' + t_type.to_s

      end
      #########################################################################
    }

  end
end
