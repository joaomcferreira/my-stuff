require 'optparse'
require 'yaml'

class Config

  #############################################
  @@options = {}
  OptionParser.new do |opt|
    opt.on('--branch'    + ' <YOUR_WORK_BRANCH>') { |o| @@options[ :branch      ] = o }
    opt.on('--dump_file' +   ' <DUMP_FILE_PATH>') { |o| @@options[ :dump_file   ] = o }
    opt.on('--test_list' +   ' <TEST_LIST_NAME>') { |o| @@options[ :test_list   ] = o }
    opt.on('--verbose'   +    ' <VERBOSE_LEVEL>') { |o| @@options[ :verbose     ] = o }
    opt.on('--n_times'   +     ' <TEST_N_TIMES>') { |o| @@options[ :test_n_times] = o }
  end.parse!

  #############################################
  def _get_cfg()
    return YAML.load_file('leemonidas.yml')
  end

  def _get_cfg_general_by_key(key)
    return _get_cfg()['general'][key]
  end

  def _get_cfg_db_host()
    return 'localhost'
  end

  def _get_cfg_db_port()
    return 5432
  end

  def _get_cfg_dump_file()
    a = _get_cfg_general_by_key('dump_file_from_HOME')
    raise 'dump file cfg not found' unless a
    return ENV['HOME'] + a.to_s
  end

  def _get_cfg_bundle_dir() 
    a = _get_cfg_general_by_key('bundle_dir_from_HOME')
    return nil unless a
    ENV['HOME'] + a
  end

  def _get_cfg_run_tests_n_times()
    return _get_cfg_general_by_key('run_tests_n_times') || 1
  end

  def _get_cfg_clone_dir()
    cfg_from_home = _get_cfg_general_by_key('clone_dir_from_HOME')
    return '/tmp/leemonidas_src' if ARGV.include?('useTmp')
    return ENV['HOME'] + '/work/leemonidas_src' if ARGV.include?('useHomeWork')
    return ENV['HOME'] +  cfg_from_home if cfg_from_home
    return ENV['HOME'] + '/leemonidas_src'
  end

  #############################################
  def get_clone_dir()
    _get_cfg_clone_dir()
  end

  def get_rrr_dir()
    _get_cfg_clone_dir() + '/' + _get_cfg_general_by_key('root_ruby_src_dir')
  end

  def get_repo_url()
    _get_cfg_general_by_key('repo') || 'no_repo_defined'
  end

  def get_tst_dir()
    ENV['HOME'] + '/leemonidas_tests'
  end

  def get_dirty_dir()
    Dir.pwd() + '/dirty_deeds'
  end

  def get_jobs_cfg_file_path()
    Dir.pwd() + '/jobs/main.conf.json'
  end

  def get_db_hp_string()
    '-h localhost -p 5432'
  end

  def get_db_ud_string()
    '-U leemonidas leemonidas'
  end

  def get_db_password()
    'leemonidas'
  end

  #############################################
  def cfg_get_verbose()
    return true if @@options[:verbose]
    return false
  end

  def cfg_get_db_host()
    _get_cfg_db_host()
  end

  def cfg_get_db_port()
    _get_cfg_db_port()
  end

  def cfg_get_dump_file()
    return @@options[:dump_file] if @@options[:dump_file]
    _get_cfg_dump_file()
  end

  def cfg_get_branch()
    return @@options[:branch] if @@options[:branch]
    _get_cfg_general_by_key('branch')
  end

  def cfg_get_run_tests_n_times()
    return @@options[:test_n_times].to_i if @@options[:test_n_times]
    _get_cfg_run_tests_n_times
  end

  def cfg_get_test_list_name()
    return @@options[:test_list] if @@options[:test_list]
    _get_cfg_general_by_key('test_list')
  end

  def cfg_get_rspec_opts()
    _get_cfg_general_by_key('rspec_opts')
  end

  def cfg_get_pull_before_bundle()
    _get_cfg_general_by_key('pull_before_bundle')
  end

  #############################################
  def _check_argument(ctx, arg, w)
    return true if 'alwaysTrue' == arg
    if ARGV.include?(arg) then
      ctx.generic.banner w + ' (as requested)'
      return true
    end
    return false
  end

  def should_skip_pre_tests(ctx)          ; _check_argument(ctx, 'noPreTests'          , 'skipping pre-tests'          ); end
  def should_skip_tests(ctx)              ; _check_argument(ctx, 'noTests'             , 'skipping tests'              ); end
  def should_stop_after_add_base_data(ctx); _check_argument(ctx, 'stopAfterAddBaseData', 'stopping after add base data'); end
  def should_skip_add_base_data(ctx)      ; _check_argument(ctx, 'alwaysTrue'          , 'will not add base data'      ); end
  def user_wants_casper_mode(ctx)         ; _check_argument(ctx, 'casperMode'          , 'casperMode: services are up' ); end

  #############################################

end
