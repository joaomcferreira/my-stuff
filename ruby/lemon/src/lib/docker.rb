class Docker

###############################################################################
def erase_ALL_docker(ctx)
  cmd = 'echo "erase ALL docker"; docker ps -aq | xargs docker stop; docker container ls -aq | xargs docker rm; docker image ls -aq | xargs docker rmi; docker image prune -af; docker system prune -f; docker volume prune -f ; docker volume ls -q | xargs docker volume rm;'
  ctx.generic.run_shell_command(cmd)
  ctx.generic.banner 'making sure !!!'
  ctx.generic.run_shell_command(cmd)
end

###############################################################################
def clear_all_docker_volumes(ctx)

  if ARGV.include?('clientMode') then
    ctx.generic.banner 'clientMode: skipping clear_all_docker_volumes'
    sleep 1
    return
  end

  ctx.generic.banner 'clearing all docker volumes'
  sleep 1
  stop_all_docker_containers(ctx)
  ctx.generic.run_shell_command 'docker system prune -f', 'prune system'
  ctx.generic.run_shell_command 'docker volume prune -f', 'prune volumes'
  ctx.generic.run_shell_command 'docker system prune -f', 'prune system again'
  ctx.generic.run_shell_command 'docker volume prune -f', 'prune volumes again'
  ctx.generic.banner ''
end

###############################################################################
def stop_all_docker_containers(ctx)
  stop_cmd = 'docker ps -aq | xargs docker stop'
  #try "docker-compose stop" to avoid stopping non-leemonidas containers that might be running
  ctx.generic.run_shell_command stop_cmd, 'Stopping all docker containers'
  ctx.generic.run_shell_command stop_cmd, 'Stopping all docker containers (again)'
  ctx.generic.run_shell_command 'docker container ls -a' , 'List all docker containers'
  sleep 1
  ctx.generic.banner ''
end

###############################################################################
def docker_start_base_command(ctx)
  'docker-compose up --build'
end

###############################################################################
def fork_start_docker_db(ctx)

  if ARGV.include?('clientMode') then
    ctx.generic.banner 'clientMode: skipping fork_start_docker_db'
    sleep 1
    return
  end

  cmd = docker_start_base_command(ctx) + ' &'
  ctx.generic.run_shell_command cmd, 'start DB (forking)'
  sleep 9
  ctx.jobs.start_jobs(ctx)#TODO_jmf; this is ugly: jobs has nothing to do with docker; think infra instead !!!
end

###############################################################################
def normal_start_docker_db(ctx)
  cmd = docker_start_base_command(ctx)
  ctx.generic.run_shell_command cmd, 'start DB (not forking)'
end

###############################################################################

end
