class PsqlX
  #############################################
  def psql_cmd_base(hp, ud, pass)
    "PGPASSWORD=#{pass} psql #{hp} #{ud}"
  end

  def psql_cmd(base, sql)
    base + ' -c "' + sql + '"'
  end

  def exec_psql_qry(ctx, hp, ud, pass, sql)
    ctx.generic.run_shell_command(psql_cmd(psql_cmd_base(hp, ud, pass), sql))
  end

  def psql_cmd_new_base(ctx)
    hp = ctx.config.get_db_hp_string()
    ud = ctx.config.get_db_ud_string()
    ps = ctx.config.get_db_password()
    psql_cmd_base(hp, ud, ps)
  end

  def exec_psql_qry_new(ctx, sql)
    ctx.generic.run_shell_command(psql_cmd(psql_cmd_new_base(ctx), sql))
  end

  def exec_many_psql_qrys(ctx, sqls)
    sqls.each { |sql| exec_psql_qry_new(ctx, sql) }
  end

  def load_sql_file_command(ctx, path)
    psql_cmd_new_base(ctx) + ' < ' + path
  end

  def load_sql_file(ctx, path)
    command = load_sql_file_command(ctx, path)
    ctx.generic.run_shell_command command
  end

  #############################################
  def wait_for_pg(host, port, max, nap, min_ok)
    command = "pg_isready -h #{host} -p #{port} -U ovelhaxone"
    puts command

    i =  1
    success = 0
    while i <= max  do
      db_is_ready = system(command)

      if db_is_ready
        success += 1
      else
        success = 0
      end

      return 0 if success >= min_ok

      puts('wait for pg (' + i.to_s + '/' + max.to_s + ')')
      sleep 9 unless db_is_ready
      sleep nap
      i +=1
    end

    puts 'no connection to pg'
    return 1
  end

  #############################################
end
