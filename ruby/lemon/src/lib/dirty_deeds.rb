class DirtyD

  #############################################################################
  def do_your_thing(ctx)

    if ARGV.include?('noDirtyDeeds') then
      ctx.generic.banner 'refusing to do any of that dirty stuff (as requested)'
      return
    end

    #this "weird and strange" usage of ctx is a generalized approach to Inversion of Control and Dependency Injection
    #suggestion: https://gitlab.com/joaomcferreira/my-stuff/-/blob/master/ideas/my_ideas_on_sw_engineering.pdf
    clone_dir = ctx.config.get_clone_dir()
    ctx.generic.exit_1_unless_dir_exists clone_dir, 'clone dir ', ''

    #hack some files (not pretty, but... life is hard)
    dirty_dir = ctx.config.get_dirty_dir()
    cmd = "cd #{clone_dir} && patch --forward -r - -p1 < #{dirty_dir}/some_patch.diff"
    ctx.generic.run_shell_command cmd, 'dirty deeds - part 2: patching some files'

    ctx.generic.banner 'done doing dirty deeds'
  end

  #############################################################################

end
