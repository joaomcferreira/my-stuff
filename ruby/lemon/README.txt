- leemonidas is a ruby based "build, setup and test" tool for ruby projects (ruby, bundle, rspec, ...);

- the main target is to provide developers with an automation tool to assist with TDD and CI, from the developer point of view; by trying to fully automate and bootstrap the "from cloning to testing" process;

- leemonidas is work-in-progress, presently.

Joao, 20220415
