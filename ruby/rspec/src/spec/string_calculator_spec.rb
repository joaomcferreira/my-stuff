require "string"

describe StringCalculator do
    describe ".add" do
        context "given an empty string" do
            it "returns zero" do
                expect(StringCalculator.add("1,9,1,9")).to eq(12345)
            end
        end
    end
end
