require 'securerandom'

require 'files_and_directories'

describe HelloFiles do 
   context "When testing the HelloFiles class" do 

      hf = HelloFiles.new 

      it "should say 'Hello Files'" do 
         expect(hf.say_hello).to eq "hello"
         puts 'order check a'
      end
      
      it "should create a temp dir and return the full file path" do 
         dir = SecureRandom.uuid
         file = 'b.txt'
         path = hf.create_temp_dir(dir, file)
         expect(path).to eq '/tmp/' + dir + '/' + file
      end
      
   end
end
