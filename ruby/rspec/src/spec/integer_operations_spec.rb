require 'integer'

describe IntegerStuff do
    subject(:uut) { IntegerStuff.new }

    it "tells you if an integer is even" do
        expect(uut.is_even(2)).to eq('no')
    end
    it "tells you if an integer is odd" do
        is_it_really_odd = uut.is_odd(2)
        expect(is_it_really_odd).to eq('yes')
        puts 'order check c'
    end
end
