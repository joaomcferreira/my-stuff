class HelloFiles

    def say_hello
        "hello"
    end

    def create_temp_dir(x, y)
        full_path = File.join('/tmp', x, y)
        dir_path = File.dirname(full_path)
        FileUtils.mkdir_p dir_path
        puts full_path
        full_path
    end

end
