require 'bunny'

connection = Bunny.new(automatically_recover: false)
connection.start

channel = connection.create_channel
queue = channel.queue('ovelha-xone-123x')

# !!!!!
#channel.prefetch(1)

begin
  n = 20
  m = 0

  while n > 0  do
    puts " [*] Checking for messages (#{n}). To exit press CTRL+C"
    n = n - 1

    queue.subscribe(block: false) do |_delivery_info, _properties, body|
      m = m + 1
      puts " [x] Received count: |#{m}|; body: |#{body}|;"
    end

    sleep 2

end


rescue Interrupt => _
  connection.close

  exit(0)
end
