require 'bunny'

connection = Bunny.new(automatically_recover: true)
connection.start

channel = connection.create_channel
queue = channel.queue('ovelha-xone-123x')

now = Time.now.to_i
msg = "Hello World #{now}"
puts msg

channel.default_exchange.publish(msg, routing_key: queue.name)
puts "   Sent #{msg} !!!"

connection.close
