require 'bunny'

connection = Bunny.new(automatically_recover: false)
connection.start

channel = connection.create_channel
queue = channel.queue('ovelha-xone-123x')

begin
  n = 0
  puts ' [*] Waiting for messages. To exit press CTRL+C'
  # block: true is only used to keep the main thread
  # alive. Please avoid using it in real world applications.
  queue.subscribe(block: true) do |_delivery_info, _properties, body|
    n = n + 1
    puts "=========================================================="
    puts " [x] Received count: |#{n}|; body: |#{body}|;"
    puts " [x] Received _properties #{_properties}"
   #puts " [x] Received _delivery_info #{_delivery_info}"
    puts "=========================================================="
  end
rescue Interrupt => _
  connection.close

  exit(0)
end
