require 'open3'

###############################################################################
def log_err (x)
  t = Time.now.to_i
  text = "[#{t}] #{x}"
  STDERR.puts(text)
  File.write('/tmp/mhr2s.log', text + "\n" , mode: 'a')
end

###############################################################################
def print_usage_and_exit
  log_err(' Usage:')
  log_err('')
  log_err('  ruby mhr2s.rb sh ACTION 192.168.1.73 cups remote_sudo_pass.txt')
  log_err('  ruby mhr2s.rb mh ACTION hosts.txt services.txt remote_sudo_pass.txt')
  log_err('  ruby mhr2s.rb sc ACTION etc/script.txt')
  log_err('')
  log_err('   ACTION can be status or restart')
  log_err('')
  exit 1
end

###############################################################################
def slurp_file (x, path)
  log_err("#{x} #{path}")
  lines = []
  File.open(path, "r") { |f|
    f.each_line { |l|
      c = l.chomp
      if l.match('^#')
        log_err("#{x} line ignored: |#{c}|")
      else
        lines.push(c)
      end
    }
  }
  lines
end

###############################################################################
def user_input_to_question (q)
  log_err('Question: ' + q)
  answer = STDIN.gets.chomp
  log_err("user response was |#{answer}|")
  answer
end

###############################################################################
def user_confirmation (q, x)
  r = user_input_to_question(q)
  x == r
end

###############################################################################
def run_shell_command (cmd, nap)
  log_err('xSHELL: ' + cmd)
  stdout, stderr, status = Open3.capture3(cmd)
  log_err("STATUS: #{status}")
  log_err('STDOUT: ' + stdout)
  log_err('STDERR: ' + stderr)
  sleep nap if nap
  stdout
end

###############################################################################
def remote_systemd_cmd (h, a, s, pf)
  cmd_c = "cat #{pf} | ssh #{h}"
  r_cmd = "cat | sudo --prompt='' -S -- systemctl #{a} #{s}"
  cmd_c + ' "' + r_cmd + '"'
end

def run_systemd_stop (h, s, pf)
  run_shell_command(remote_systemd_cmd(h, 'stop', s, pf), 1)
end

def run_systemd_start (h, s, pf)
  run_shell_command(remote_systemd_cmd(h, 'start', s, pf), 1)
end

def get_systemd_status (h, s, pf)
  log_err("check status of |#{s}| on |#{h}|")
  run_shell_command(remote_systemd_cmd(h, 'status', "#{s} | grep Active", pf), nil)
end

###############################################################################
def wait_for_systemd_status (h, svc, sts, max, pf)
  nap = 1
  count = 0
  total_matches = 0

  while ((total_matches < 2) && (count < max))
    count = count + 1
    line = get_systemd_status(h, svc, pf)

    if line.match(sts)
      total_matches = total_matches + 1
    else
      total_matches = 0
    end

    log_err("wait for [#{svc}] systemd status line to match [#{sts}] on #{h} [#{total_matches}/#{count}/#{max}]")
    sleep nap if nap
  end

  res = 'FAILED'
  res = 'ok' if count < max
  log_err("service |#{svc}| status |#{sts}|: #{res}")

  res == 'ok'
end

###############################################################################
def restart_one_host_one_service (host, svc, dont_ask_questions, pf)

  unless dont_ask_questions
    return { status: true, reason: 'user_skip' } unless
      user_confirmation("this will restart service (#{svc}) on host (#{host}). proceed ? (y, n)",'y')
  end

  success_x = wait_for_systemd_status(host, svc, ' active', 5, pf)#the space is needed because 'inactive' matches 'active'
  unless success_x
    log_err("service |#{svc}| is NOT running. refusing to touch it.")
    return { status: false, reason: 'was_weird' } if dont_ask_questions
    return { status: false, reason: 'weird_dont_force' } unless user_confirmation("service |#{svc}| seems weird. force restart ? (y, n)",'y')
  end

  run_systemd_stop(host, svc, pf)
  success_a = wait_for_systemd_status(host, svc, 'dead', 20, pf)

  run_systemd_start(host, svc, pf)
  success_b = wait_for_systemd_status(host, svc, ' active', 20, pf)#the space is needed because 'inactive' matches 'active'

  return { status: false, reason: 'found_errors' } unless success_a && success_b
  { status: true, reason: 'ok' }
end

###############################################################################
def print_statuses_one_host_many_services (host, svcs, pf)
  log_err("check statuses of |#{svcs}| on host #{host}")
  svcs.each { |svc| get_systemd_status(host, svc, pf) }
end

###############################################################################
def restart_one_host_many_services (host, svcs, no_questions_please, pf)

  dont_ask_questions = no_questions_please

  unless dont_ask_questions
    ui = user_input_to_question("this will restart services (#{svcs}) on host #{host}. proceed ? (a, y, n)")

    return [], [
      { t: Time.now.to_i, status: 'ok', host: host, svc: 'ALL', reason: 'user_skip' }
        ] unless ui == 'a' || ui == 'y'

    dont_ask_questions = ui == 'a'
  end

  failures = []
  summary = []

  svcs.each { |svc|
    t = Time.now.to_i
    sum = { t: t, status: 'ok', host: host, svc: svc }
    res = restart_one_host_one_service(host, svc, dont_ask_questions, pf)

    if res[:status]
      log_err("no issues detected on service #{svc}")
    else
      sum[:status] = 'KO'
      failures.push(svc)
      log_err("service #{svc} had problems stopping or re-starting")
    end

    sum[:reason] = res[:reason]
    summary.push(sum)
  }

  if failures.length > 0
    log_err("the following services failed stopping or re-starting: |#{failures}|.")
  else
    log_err('no service stop/start failures detected. all seems fine.')
  end

  return failures, summary
end

###############################################################################
def restart_and_retry_one_host_many_services (host, svcs, no_questions_please, pf)

  full_summary = []

  failures, summary = restart_one_host_many_services(host, svcs, no_questions_please, pf)

  full_summary.push(summary)

  unless no_questions_please
    while (failures.length > 0)
      break unless user_confirmation('try to restart the failed services again ? (y, n)', 'y')
      failures, summary = restart_one_host_many_services(host, failures, no_questions_please, pf)
      full_summary.push(summary)
    end
  end

  full_summary
end

###############################################################################
def run_action (action, path_hosts, path_services, path_sudo_pass)
  hosts = slurp_file('loading hosts -------> ', path_hosts)
  svcs  = slurp_file('loading services ----> ', path_services)
  pass  = slurp_file('checking sudo file --> ', path_sudo_pass)#just to ensure we have a sane sudo password file

  full_summary = []

  if ('restart' == action)
    ui = user_input_to_question("this will restart services (#{svcs}) on hosts (#{hosts}). proceed ? (a, y, n)")
    return unless ui == 'a' || ui == 'y'

    hosts.each { |h|
      summary = restart_and_retry_one_host_many_services(h, svcs, ui == 'a', path_sudo_pass);
      full_summary.push(summary)
    }

    log_err("==============================")
    log_err("summary:")
    log_err("--> #{action}; #{path_hosts}; #{path_services}; #{path_sudo_pass};")
    log_err("==============================")
    full_summary.flatten.each { |a| log_err(a) }
    log_err("==============================")
    log_err("")

    return full_summary

  elsif ('status' == action)
    hosts.each { |h| print_statuses_one_host_many_services(h, svcs, path_sudo_pass) }
    return 'TODO_b'

  end

  raise "action #{action} not supported (this should not happen... means programmer error !!! :) )"
end

###############################################################################
def exec_script(action, script_file)
  log_err("script mode #{action} #{script_file}");
  script_objects = []
  script_lines = slurp_file('loading script -->', script_file)
  script_lines.each { |sl|
    sl_h_f, sl_s_f, sl_p_f = sl.split
    log_err(sl)
    log_err(sl_h_f)
    log_err(sl_s_f)
    log_err(sl_p_f)
    if sl_h_f && sl_s_f && sl_p_f
      log_err("this line is cool. will use it.")
      script_objects.push( { hf: sl_h_f, sf: sl_s_f, pf: sl_p_f } )
    else
      log_err("this line is weird. will ignore it.")
    end
  }

  summary = []
  script_objects.each { |o|
    log_err("script mode")
    r = run_action(action, o[:hf], o[:sf], o[:pf])
    summary.push(r)
  }

  log_err("--------------------------------")
  log_err("script mode summary:")
  log_err("--> #{action}; #{script_file};")
  log_err("--------------------------------")
  summary.flatten.each { |a| log_err(a) }
  log_err("--------------------------------")
end

###############################################################################
def decide_what_to_do_and_do_it

  if ARGV[0] == 'help'
    print_usage_and_exit()
  elsif ARGV[0] == 'sh' || ARGV[0] == 'single'
  elsif ARGV[0] == 'mh' || ARGV[0] == 'multi'
  elsif ARGV[0] == 'sc' || ARGV[0] == 'script'
  else
    log_err("argument 0 (#{ARGV[0]}) not supported.")
    print_usage_and_exit()
  end

  unless ARGV[1] == 'status' || ARGV[1] == 'restart'
    log_err("argument 1 (#{ARGV[1]}) not supported.")
    print_usage_and_exit()
  end

  if ARGV[0] == 'sh' || ARGV[0] == 'single'
    print_usage_and_exit() unless ARGV[2] && ARGV[3] && ARGV[4]
    print_usage_and_exit() if ARGV[5]

    if ARGV[1] == 'restart'
      restart_one_host_one_service(ARGV[2], ARGV[3], false, ARGV[4])
    else
     #print_statuses_one_host_many_services(ARGV[2], [ ARGV[3] ], ARGV[4])
      get_systemd_status(ARGV[2], ARGV[3], ARGV[4])
    end

  elsif ARGV[0] == 'mh' || ARGV[0] == 'multi'
    print_usage_and_exit() unless ARGV[2] && ARGV[3] && ARGV[4]
    print_usage_and_exit() if ARGV[5]
    run_action(ARGV[1], ARGV[2], ARGV[3], ARGV[4])

  elsif ARGV[0] == 'sc' || ARGV[0] == 'script'
    exec_script(ARGV[1], ARGV[2])

  end

end

###############################################################################
decide_what_to_do_and_do_it()
