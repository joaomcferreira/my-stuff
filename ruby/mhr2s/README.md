mhr2s - Multiple Host Restart System Services
=====

**mhr2s** restarts systemd services on remote hosts, over ssh.

**mhr2s** assumes the usage of sudo on the remote host. The sudo password is piped to sudo's stdin, through the ssh tunnel. The password is never written or visible anywhere, except locally on the password file.

See also: *[sudo over SSH](https://gitlab.com/joaomcferreira/my-stuff/-/tree/master/misc/sudo_ssh)*


Usage
-----

* `ruby mhr2s.rb help`

* `ruby mhr2s.rb sh ACTION john@192.168.1.73 cups remote_sudo_pass.txt`

* `ruby mhr2s.rb mh ACTION hosts.txt services.txt remote_sudo_pass.txt`

* `ruby mhr2s.rb sc ACTION etc/script.txt`

ACTION can be 'status' or 'restart':

* **status** causes mhr2s to echo the status of the remote service as reported by the remote systemd utility

* **restart** causes mhr2s to restart services on the remote host using systemd. services are restarted in a gentle, one-by-one, orderly manner, always under control of the user. if a service is not running it will not be restarted unless the user explicitly requests it.


Operation Modes
---------------

* **sh - single host mode**: checks/restarts a single service on a single host

* **mh - multiple host mode**: checks/restarts all services listed in the services file passed as argument on each of the hosts listed in the hosts file passed as argument

* **sc - script mode**: reads the script file, passed as argument, containing lines composed as arguments for **mh** mode and executes them one by one.


Log file
---------------

mhr2s logs everything to **/tmp/mhr2s.log**


Usage examples
--------------

![sample_usage_01](img/sample_usage_01.png)

![sample_usage_02](img/sample_usage_02.png)

![sample_usage_03](img/sample_usage_03.png)

