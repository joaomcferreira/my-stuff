require 'sinatra/base'

module SinatraCtx

    def SinatraCtx.get_instance(x)
        app = Sinatra.new do
          #####################################################################
          set :bind, '0.0.0.0'#bind all interfaces; needed for Docker
          #####################################################################
          get('/test1') {
            x.ctx('logger').log_info('user asked for /test1')
            '<hr>!!! xone123x !!!<hr>'
          }
          #####################################################################
          get('/test2') {
            name = x.ctx('db_reader').get_musician_by_style('swing')
            x.ctx('logger').log_info('musician name is .....')#Chuck Berry
            name
          }
          #####################################################################
          get('/test3') {
            x.ctx('logger').log_info('user asked for /test3')
            x.ctx('db_reader').get_musician_by_style('classical')#Glenn Miller
          }
          #####################################################################
        end
    end

end
