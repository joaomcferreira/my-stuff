require_relative '../util/my_logger_class'
require_relative '../util/database_reader'

class XCtx
    #############################################
    private
    @@tools = {}
    def _instance (x)
        if x
            if 'logger' == x
                return MyLoggerClass.new
            elsif 'printer' == x
                return MyLoggerClass.new
            elsif 'db_reader' == x
                return DatabaseReader.new
            end
        end
        raise "this_is_an_exception"
    end

    #############################################
    public
    def force (a, whatever)
        @@tools[a] = whatever
    end
    def ctx (a)
        @@tools[a] ||= _instance(a)
    end

    #############################################
end
