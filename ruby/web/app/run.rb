#
#run: "ruby run.rb"
#
#docker run -p 4567:4567 sinatra-test
#

require_relative 'lib/app/xctx.rb'
require_relative 'lib/web/sinatra_ctx.rb'

#application context
x = XCtx.new

#server with injected application context
x.ctx('logger').log_info 'setting up...'
server = SinatraCtx.get_instance(x)

#run it
x.ctx('logger').log_info 'starting...'
server.run!
