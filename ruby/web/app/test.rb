require_relative 'lib/app/xctx.rb'

x = XCtx.new
x.ctx( 'logger').log_info("__logger ok")
x.ctx('printer').log_info("_printer ok")

x.force('xone', x.ctx( 'logger'))
x.ctx( 'xone').log_info("____xone ok")

sleep 2
x.ctx('foo').log_info("____foo KO")
