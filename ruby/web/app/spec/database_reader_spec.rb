require 'util/database_reader'

describe DatabaseReader do
    describe ".get_musician_by_style" do
        context "given the style rock" do
            it "returns Chuck Berry" do
                dr = DatabaseReader.new
                expect(dr.get_musician_by_style('rock')).to eq('Chuck Berry')
            end
        end
        context "given the style swing" do
            it "returns Glenn Miller" do
                dr = DatabaseReader.new
                expect(dr.get_musician_by_style('swing')).to eq('Glenn Miller')
            end
        end
    end
end
