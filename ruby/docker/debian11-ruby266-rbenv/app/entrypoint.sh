#!/bin/bash -l
echo '----> /usr/bin/ruby -v (system ruby)'
/usr/bin/ruby -v
echo '---------------'
echo '----> echo $PATH'
echo $PATH
echo '---------------'
echo '----> rbenv versions'
rbenv versions
echo '---------------'
echo '----> rbenv global'
rbenv global
echo '---------------'
echo '--> which ruby'
which ruby
echo '---------------'
echo '----> ruby -v (rbenv ruby)'
ruby -v
echo '----! the previous command should print something like ruby 2.6.6 or similar'
echo '---------------'
echo '----> bundle and bundle check'
cd /app && bundle && bundle check
echo '----! the previous command should print something like Gemfile dependencies ok'
echo '---------------'
echo '----> bundle info'
cd /app && bundle info rspec
echo '----! the previous command should print the path to the rspec gem (/root/.rbenv/.....)'
echo '---------------'
echo "=============================================================="
echo "=============================================================="
uname -a
grep PRETTY /etc/os-release
echo "=============================================================="
echo "=============================================================="
echo "test: test/unit" && ruby /app/basic_test.rb && echo "test: rspec" && bundle exec rspec && echo "all tests seem to pass" && echo "========== cool =========="
