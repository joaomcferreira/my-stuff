ms = [
    "amqp",
    "bundler",
    "byebug",
    "colorize",
    "bunny",
    "minitest",
    "os",
    "pg",
    "rack",
    "simplecov",
    "sinatra",
    "test/unit",
    "thor",
]

ms.each do |m|
   puts "require #{m}"
   require m
end

#
#
require "echo_test"
describe EchoTest do
    it "echoes back whatever you say" do
        puts "simple echo test"
        a = "Good Morning"
        b = EchoTest.echo(a)
        expect(b).to eq(a)
    end
end
#
