Notes about ruby
============
I'm new to ruby, learning as I move forward. I use this MD file to take notes. Nothing special, just... my_ruby_stuff.

------------
I - Debian/Ubuntu
------------
* `apt install ruby ruby-full ruby-rspec ruby-bundler`

-----------
II - Gemfile
-----------
Your project may contain a Gemfile. That file lists your project's dependencies on other projects or software libraries.

Your project needs additional stuff (other than the ruby interpreter it-self). Those dependencies are listed in the Gemfile.

------------
III - bundle
------------
The bundle command comes with the bundler package and it is used to install gems listed in your project's Gemfile.

### installation
* `apt install ruby-bunlder`
* `bundle -v`
### usage
* `cd your_ruby_project && bundle`
    * this will read the project's Gemfile and install the required gems
    * usually from https://rubygems.org
    * in some cases the Gemfile loads other related files


### .bundle/config
The following is a sample of the bundler config file:

![bundle config file](bundle_config.png)

You can simply edit it by hand, or:

* `bundle config set path 'my/local/bundle'`
* `bundle install --path .bundle`
    * the previous commands will create a setting in you home folder that will make all subsequent calls to "bundle" use the specified folder to install gems
    * the created file is `$HOME/.bundle/config`
    * you can also have that setup ('.bundle/config') in your repo's root; if so, that setting will override the setting in $HOME
* this way bundle will install the needed gems into a local directory and thus avoid global impact or anoyance to other projects (this could be good or bad - your choice)
* also usefull in situations where you/bundle can not write to system scoped directories (like /usr/local, for example) tipically in Linux
* somewhat similar to "gemsets" available in rvm
* in some weird scenario I got "BUNDLE_SET: "path my/local/bundle" and it messed things up. the correct thing is 'BUNDLE_PATH: "...."' and not "BUNDLE_SET"

### bundle exec
* `bundle exec ruby my_test.rb`
* `bundle exec rake -T`
* bundle knows where to find the gems he just installed (he should know); `bundle exec ...` will set things up in order for other programs to be able to run without knowing. That's cool

### bundle update
* `bundle update --source <gem_name>`

------------
IV - binary linked gems
------------
Some gems are implemented as wrappers for existing binary compiled code. The related binary/dev packages need to be in place:

* `apt install ruby-dev` (required to install some gems: byebug, bcrypt, ...)
* `apt install libpq-dev` (needed by the <u>pg</u> gem - ruby's PostgreSQL Client)
* `apt install zlib1g-dev` (required by the <u>nokogiri</u> gem to build bindings for libxml2)
* `apt install libxml2-dev` (for the <u>libxml-ruby</u> gem - a ruby binding for libxml2)
* `apt install libncurses-dev` (required by the <u>ruby-terminfo</u> gem)
* `apt install libsqlite3-dev` (required by the <u>sqlite3</u> gem)
* `apt install libmagick++-dev` (required by the <u>rmagick</u> gem)
* `apt install libcurl4-openssl-dev` (for the <u>curb</u> gem - a ruby binding for libcurl)
* `apt install gcc make build-essential` (needed for building, example bcrypt)

Similar requirements apply to other Linux systems. Examples bellow:

* Fedora33
    * `yum install libpq-devel`
    * `yum install libcurl-devel`
    * `yum install zlib-devel`
    * `yum install libxml2-devel`
    * `yum install ncurses-devel`
    * `yum install sqlite-devel`
    * `yum install ruby-devel`
    * `yum install redhat-rpm-config`
    * `yum install gcc make gcc-c++`

------------
V - rspec
------------
rspec supports the following options (command line or .rspec file)

* `--fail-fast`
* `--order rand`
* `--exclude-pattern "spec/excluded/*"`
* `--format documentation` (same as "-f d")

------------
VI - rvm
------------
rvm seems a weird idea for me: I find a bit scary the idea that I might need two versions of the same interpreter on the same computer (hacking, experimentations, new solutions... maybe). Well, let's see what happens

* Despite it seems weird I decided to give it a try on a fresh Debian 11, without any APT related ruby on it. "rvm.io" has all the installation details and I was able to create an "rvm based" ruby environment with only ruby-2.6.6 and bundler-2.2.15. ruby was installed by running "rvm install 2.6.6" as root. bundler was installer by running "gem install bundler", not as root. Seems fine so far. Will see how it goes.

* Another scenario: Debian 10, without any APT related ruby stuff, installed rvm from rvm.io. Added my user to the sudo group (/etc/group) followed by logout/login. No root was needed. After that "rvm install 2.6.6" and added .rvm/rubies/ruby-2.6.6/bin to PATH on .bashrc. In this setup bundler is at version 1.17.3 (as shipped by rvm). Seems fine.

* And yet another scenario: same as previous but this time with a Debian 11 in exact same initial conditions. ruby 2.6.6, bundler 1.17.3. Also seems fine.


I have not yet tried any situation with 2 distinct versions of the interpreter on the same computer. I use VMs and I deploy a single version on each. Some with rvm, some with APT.

------------
VII - rbenv
------------

A bit idealisticly I still feel that having several versions of the same interpreter on the same computer seems weird. But daily life has a strength of its own and this kind of things becomes necessary for a number of reasons. So I'm trying to setup a flexible ruby environment with the help of rbenv on a Debian 11 computer. These were my steps:

* apt install rbenv (sudo/root required here)
* compilers may be necessary (suggestions: "apt install build-essential", "apt install gcc", sudo/root required)

From here on sudo/root is no longer required

* make sure your "echo $PATH" contains "/home/james/.rbenv/shims" at the left (example: "/home/james/.rbenv/shims:/usr/local/bin:/usr/bin")
* rbenv install 2.6.6 (installs ruby 2.6.6)
* rbenv global 2.6.6 (sets ruby 2.6.6 to be the global ruby interpreter)
* rbenv global (check the previous setting)
* ruby -v (to make sure)

On any subsequent reboot and login, anything user james executes with ruby will be using ruby 2.6.6. If I login as root, or any other user, none of this is in place and the ruby interpreter version will be the one on the system (2.7 for Debian 11).

This approach also handles older systems nicelly: I was able to setup ruby-2.6.9 on an Ubuntu 18.04 system. For this case the ruby version shipped by apt is 2.5.1. The highest availabe ruby version throught the apt available rbenv was 2.4.1. So I tried cloning their git repo and just followed the instructions and everything works fine. The relevant information can be found here: 'https://github.com/rbenv/rbenv' and here: 'https://github.com/rbenv/ruby-build' ('apt install libreadline-dev' may be needed).

------------
VIII - misc stuff
------------

root@deb10tp:~# gem uninstall bundler

Successfully uninstalled bundler-2.1.4

root@deb10tp:~# apt install ruby-colorize

root@deb10tp:~# apt install ruby-byebug

root@deb10tp:~# apt install ruby-awesome-print

root@deb10tp:~# apt install rubygems-integration

root@deb10tp:~# apt install ruby-foreman

root@deb10tp:~# gem install foreman

------------
MCMXCID - RnR - really not ruby...
------------
Topics that do not fit a "strict ruby" document... but I have no place else to put if for now ;)

### ssh Local Port-Forward
* makes a local port appear to be at the remote host
* `ssh -L 5432:localhost:5432 jmf@192.168.1.84`

### ssh Remote Port-Forward
* makes a remote port appear to be at the local host
* `ssh -R 8080:localhost:80 jmf@192.168.1.84`

### sshfs
* `apt install sshfs`
* `sshfs sherlok@192.168.1.33:/Users/sherlok/work /home/watson/remote_holmes_work`

------------
MCCCXLIX - ...
------------
... and whatever additional rubies or gem stones I might dig out

------------
