require 'httpclient'
require_relative '../util/my_logger_class.rb'

class Ctx
    #############################################
    @@logger = MyLoggerClass.new
    def logger
        @@logger
    end
    #############################################
    def ua
        @@ua ||= HTTPClient.new
    end
    #############################################
end
