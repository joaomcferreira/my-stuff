# hello
puts "hello"

=begin  
 Ruby Numbers  
 Usual operators:  
 + addition  
 - subtraction  
 * multiplication  
 / division  
=end  
  
puts 1 + 2
puts 2 * 3
# Integer division  
# When you do arithmetic with integers, you'll get integer answers  
puts 3.0000 / 2
puts 10 - 11
puts 1.5 / 2.6

puts '=================== hello again 1'

# In Ruby, everything you manipulate is an object  
x = 'Ruby'.length
puts x
puts '=================== hello again 2'

hash_tk = {
  "name" => "Leandro",
  "nickname" => "Tk",
  "nationality" => "Brazilian"
}

puts "My name is #{hash_tk['name']}"
puts "But you can call me #{hash_tk["nickname"]}"
puts "And by the way I'm #{hash_tk["nationality"]}"

puts '=================== hello again 3'
