class Movie
  attr_accessor :title, :year, :runtime, :box_office, :oscar_noms
  
  def initialize(
    title:, 
    year: "19XX", 
    oscar_noms: "0?", 
    runtime: "Longer than an hour", 
    box_office: "More than a million?"
  )
  
    @title = title
    @year = year
    @runtime = runtime
    @box_office = box_office
    @oscar_noms = oscar_noms
  end
end

matrix = Movie.new(
  year: 1999, 
  title: "The Matrix", 
  runtime: 150,
  oscar_noms: 4,
  box_office: 463517383,
)

puts matrix.year
puts matrix.title

other = Movie.new(
  title: "xone",
  year: 1999, 
  box_office: 463517383
)

puts other.year
puts other.title
