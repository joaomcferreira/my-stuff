#
#$LOAD_PATH << './lib/'
#require 'my_logger'
#
#require './lib/my_logger.rb'
#
require 'pp'
require_relative 'lib/util/my_logger_module.rb'
require_relative 'lib/util/my_logger_class.rb'
require_relative 'lib/app/ctx.rb'

r = MyLoggerModule.log_error('hello')
r = MyLoggerModule.log_debug('world')
puts r

logger = MyLoggerClass.new
r = logger.log_info 'aaaaa'
puts r

ctx = Ctx.new
r = ctx.logger.log_info 'xone123a'

stuff = {
  "logger_thing1" => logger,
  "logger_thing2" => MyLoggerClass.new,
  "name" => "Joao",
}

r = stuff["logger_thing1"]
puts r
r.log_info 'zzz1'

r = stuff["logger_thing2"].log_info 'zzz2'

r = ctx.ua.get 'http://www.debian.org'
#pp(r)
pp('status code1:', r.http_header.status_code)
r = ctx.ua.get 'http://www.debian.org'
#pp(r)
pp('status code2:', r.http_header.status_code)
