use strict;
use warnings;
use Data::Dumper;
use Test::More;

use lib 'lib';
use MQ::Interface;

my $test_topic = 'farm-tasks-for-schawn-' . time;
my $mq_interface = MQ::Interface->new();

foreach my $type ('noqueue', 'kafkax', 'kafka', 'rabbitmq') {
    sleep 1;
    my $tx = 'catch_the_chickens_' . time;
    my $handle = $mq_interface->setup($type);
    $mq_interface->connect($handle, 'localhost', undef, undef);
    $mq_interface->tx_one_message($handle, $test_topic, $tx);
    sleep 1;
    my $rx = $mq_interface->rx_one_message($handle, $test_topic);
    sleep 1;
    is($rx, $tx, 'received message equals transmited message (' . ref($handle) . ')');
};

sleep 1;

done_testing();
