use strict;
use warnings;
use Data::Dumper;
use Test::More;

use lib 'lib';
use MQ::Interface;

my $mq_interface = MQ::Interface->new();
my $mq_handle = $mq_interface->setup('rabbitmq');

print STDERR Dumper $mq_interface;
print STDERR Dumper $mq_handle;

my $topics = undef;
$mq_interface->connect($mq_handle, 'localhost', 8888, $topics);
$mq_interface->tx_one_message($mq_handle, 'farm-tasks-for-schawn', 'feed the pigs');
sleep 1;
my $rx = $mq_interface->rx_one_message($mq_handle, 'farm-tasks-for-schawn');

is($rx, 'feed the pigs', 'received message ok');

done_testing();
