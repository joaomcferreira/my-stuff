package MQ::Interface;
use strict;
use warnings;

use MQ::q_RabbitMQ;
use MQ::q_NoQueue;
use MQ::q_Kafka;
use MQ::q_KafkaX;

use Moo;
use experimental qw/signatures/;

sub setup ($self, $type) {
    return MQ::q_RabbitMQ->new() if 'rabbitmq' eq $type;
    return MQ::q_NoQueue->new()  if 'noqueue'  eq $type;
    return MQ::q_Kafka->new()    if 'kafka'    eq $type;
    return MQ::q_KafkaX->new()   if 'kafkax'   eq $type;
    die "MQ type $type not supported";
};

sub connect ($self, $handle, $host, $port, $topics) {
    return $handle->_connect($host, $port, $topics);
};

sub tx_one_message ($self, $handle, $topic, $message) {
    return $handle->_tx_one_message($topic, $message);
};

sub rx_one_message ($self, $handle, $topic) {
    return $handle->_rx_one_message($topic);
};

1;
