package MQ::q_KafkaX;
use strict;
use warnings;
use Data::Dumper;

use Kafka::Connection;
use Kafka::Producer;
use Kafka::Consumer;
 
use Moo;
use experimental qw/signatures/;

my $brokers = 'localhost:9092';
my $timeout = 1111;

sub _connect ($self, $host, $port, $topics) {
    return 1;
};

sub _tx_one_message ($self, $topic, $msg) {
    print STDERR "[tx][$topic] $msg\n";
    my $c = Kafka::Connection->new( host => 'localhost' );
    my $p = Kafka::Producer->new( Connection => $c );
    $p->send($topic, 0, $msg);
    return 1;
};

sub _rx_one_message ($self, $topic) {
    my $k = Kafka::Connection->new( host => 'localhost' );
    my $c = Kafka::Consumer->new( Connection => $k );
    my $MY_MAX_BYTES = 200;
    my $messages = $c->fetch($topic, 0, 0, $MY_MAX_BYTES);
#   print STDERR Dumper $messages;
# $VAR1 = [
#           bless( {
#                    'Timestamp' => -1,
#                    'key' => '',
#                    'offset' => 0,
#                    'Attributes' => 0,
#                    'error' => '',
#                    'next_offset' => 1,
#                    'MagicByte' => 1,
#                    'HighwaterMarkOffset' => 1,
#                    'valid' => 1,
#                    'payload' => 'catch_the_chickens_1605269430'
#                  }, 'Kafka::Message' )
#         ];
    my $message = 'foo';
    $message = $messages->[0]->{payload};
    print STDERR "[rx][$topic] $message\n";
    return $message;
};

1;
