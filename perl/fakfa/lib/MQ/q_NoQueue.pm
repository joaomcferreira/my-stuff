package MQ::q_NoQueue;
use strict;
use warnings;

use Moo;
use experimental qw/signatures/;

my @queue = ();

sub _connect ($self, $host, $port, $topics) {
    return 1;
};

sub _tx_one_message ($self, $topic, $message) {
    print STDERR "[tx][$topic] $message\n";
    push @queue, $message;
    return 1;
};

sub _rx_one_message ($self, $topic) {
    my $message = shift @queue;
    print STDERR "[rx][$topic] $message\n";
    return $message;
};

1;
