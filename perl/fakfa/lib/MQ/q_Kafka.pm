package MQ::q_Kafka;
use strict;
use warnings;
use Data::Dumper;
use Kafka::Librd;

use Moo;
use experimental qw/signatures/;

my $brokers = 'localhost:9092';
my $timeout = 1111;

sub _connect ($self, $host, $port, $topics) {
   #$brokers = $host;
    return 1;
};

sub _tx_one_message ($self, $topic, $msg) {
    print STDERR "[tx][$topic] $msg\n";
    my $kafka_producer = Kafka::Librd->new(
        Kafka::Librd::RD_KAFKA_PRODUCER,
            {
                'partitioner'        => 'consistent_random',
                'enable.idempotence' => 'true',
                'compression.codec'  => 'snappy',
            }
    );

    $kafka_producer->brokers_add($brokers);
    my $topic_object = $kafka_producer->topic($topic, {});
    my $key = time;
    my $err = $topic_object->produce(Kafka::Librd::RD_KAFKA_PARTITION_UA, 0, $msg, $key);
    die $err if $err;
    return 1;
};

sub _rx_one_message ($self, $topic) {
    my $kafka_consumer = Kafka::Librd->new(
        Kafka::Librd::RD_KAFKA_CONSUMER, { 
            "group.id"             => 'group_123x',
            "auto.offset.reset"    => 'beginning',
            "enable.auto.commit"   => 'false',
            "queued.min.messages"  => 1,
            "max.poll.interval.ms" => 10000,
        }
    );
    
    $kafka_consumer->brokers_add($brokers);
    $kafka_consumer->subscribe([ $topic ]);
    my $max = 5;
    my $message;
    while ( ($max > 0) && (not $message) ) {
        sleep 1;
        $max--;

        my $msg = $kafka_consumer->consumer_poll($timeout);
        unless ($msg) {
            print STDERR "msg undefined\n";
            next;
        };

        print STDERR Dumper $msg;

        if ($msg->err) {
            print STDERR Dumper("kafka error: ", { data => { error => Kafka::Librd::Error::to_string($msg->err) } } );
            print STDERR "bad msg\n";
            next;
        };
    
        print STDERR Dumper( { key => $msg->key, partition => $msg->partition, offset => $msg->offset } );#topic???

        $message = $msg->payload;
        print STDERR Dumper $msg->payload;
        print STDERR "msg ok\n";

        $kafka_consumer->commit_message($msg, 1);
    };

    print STDERR "\n";
    print STDERR "unsubscribe\n";
    $kafka_consumer->unsubscribe;
    
    print STDERR "[rx][$topic] $message\n";
    return $message;
};

1;
