package MQ::q_RabbitMQ;
use strict;
use warnings;

use Net::AMQP::RabbitMQ;

use Moo;
use experimental qw/signatures/;

my $rabbitmq_host;
my $rabbitmq_port;

sub _connect ($self, $host, $port, $topics) {
    $rabbitmq_host = $host;
    $rabbitmq_port = $port;
    return 1;
};

sub _tx_one_message ($self, $topic, $message) {
    my $mq = Net::AMQP::RabbitMQ->new();
    $mq->connect($rabbitmq_host, { user => 'guest', password => 'guest' });

    $mq->channel_open  (1);
    $mq->queue_declare (1, $topic);
    $mq->publish       (1, $topic, $message);

    $mq->disconnect();

    print STDERR "[tx][$topic] $message\n";
    return 1;
};

sub _rx_one_message ($self, $topic) {
    my $mq = Net::AMQP::RabbitMQ->new();
    $mq->connect($rabbitmq_host, { user => 'guest', password => 'guest' });

    $mq->channel_open  (1);
    $mq->queue_declare (1, $topic);
    my $got = $mq->get (1, $topic);

    $mq->disconnect();

    my $message = $got->{body};
    print STDERR "[rx][$topic] $message\n";
    return $message;
};

1;
