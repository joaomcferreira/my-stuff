This project is a simple ilustration of how several types of Message Queues could be abstracted behind a common interface in order to avoid a programmer need to change his core code when changing the MQ he is using.

The demonstration runs in the form of a test that uses a few different MQs in the exact same scenario (a simple foreach cycle).

Run "docker-compose up" before starting or everything will fail.

In a real world situation the queue handle shall be instantiated outside by the caller and injected into the code requiring the MQ it in the form of an interface.

Cheers
Joao

this looks good: https://github.com/roribio/alpine-sqs
