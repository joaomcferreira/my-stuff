use strict;
use warnings;
use Data::Dumper;

use Kafka::Librd;


my $topic = 'tarefas-da-ovelha-xone-2';#'vlad-releases';#'search-product-wpt'

my $kafka_producer = Kafka::Librd->new(
    Kafka::Librd::RD_KAFKA_PRODUCER,
    {
        'partitioner'        => 'consistent_random',
        'enable.idempotence' => 'true',
        'compression.codec'  => 'snappy',
    }
);

print STDERR Dumper $kafka_producer;

#y $c = $kafka_producer->brokers_add("$br1,$br2,$br3");
#y $c = $kafka_producer->brokers_add("$br1");
my $c = $kafka_producer->brokers_add('localhost:9092');
#y $c = $kafka_producer->brokers_add('www.google.fr:9092');
print STDERR Dumper $c;

my $topic_object = $kafka_producer->topic($topic, {});
print STDERR Dumper $topic_object;

my $message1 = 'dar_comida_aos_porcos';
my $message2 =      'limpar_o_celeiro';
my $message3 =   'apanhar_as_galinhas';
my $base_key = 12345000000000000000;


print "===== 1 ======================================\n";
my $key = $base_key + time;
print STDERR $key . "\n";
my $err1 = $topic_object->produce(Kafka::Librd::RD_KAFKA_PARTITION_UA, 0, $message1, $key);
print STDERR Dumper $err1;
my $z1 = $kafka_producer->flush(5000);
print STDERR Dumper $z1;
print "===== 1 ======================================\n";
sleep 3;

print "===== 2 ======================================\n";
$key = $base_key + time;
print STDERR $key . "\n";
my $err2 = $topic_object->produce(Kafka::Librd::RD_KAFKA_PARTITION_UA, 0, $message2, $key);
print STDERR Dumper $err2;
my $z2 = $kafka_producer->flush(6000);
print STDERR Dumper $z2;
print "===== 2 ======================================\n";
sleep 3;

print "===== 3 ======================================\n";
$key = $base_key + time;
print STDERR $key . "\n";
my $err3 = $topic_object->produce(Kafka::Librd::RD_KAFKA_PARTITION_UA, 0, $message3, $key);
print STDERR Dumper $err3;
my $z3 = $kafka_producer->flush(5500);
print STDERR Dumper $z3;
print "===== 3 ======================================\n";
sleep 10;
