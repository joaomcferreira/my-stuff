use strict;
use warnings;
use Data::Dumper;

use Kafka::Librd;

print STDERR "===== x ===================================\n";
my $kafka_consumer = Kafka::Librd->new(
    Kafka::Librd::RD_KAFKA_CONSUMER, { 
        "group.id"             => 'group_idzz',
        "auto.offset.reset"    => 'beginning',
        "enable.auto.commit"   => 'false',
        "queued.min.messages"  => 1,
        "max.poll.interval.ms" => 10000,
    }
);

my $c = $kafka_consumer->brokers_add('localhost:9092');
print STDERR Dumper $kafka_consumer;
print STDERR Dumper $c;
print STDERR "===== x ===================================\n";

sleep 5;

my $topic = 'tarefas-da-ovelha-xone-2';#'vlad-releases';#'search-product-wpt';#'aaaaaaaaaaaaaaaaaaaaa';#

print STDERR "===== subscribe ? =================================\n";
$kafka_consumer->subscribe([ $topic ]);
print STDERR "===== subscribe . =================================\n";

my $max = 20;
my $count = 0;

while ($max > $count) {
    sleep 3;
    $count++;
    print STDERR "===== msg ? ===================================\n";
    my $msg = $kafka_consumer->consumer_poll(1000);
    unless ($msg) {
        print STDERR "===== msg undef ================================\n";
        next;
    };
    print STDERR Dumper $msg;
    print STDERR "===== msg ... =================================\n";

    if ( $msg->err ) {
        print STDERR Dumper("kafka error: ", { data => { error => Kafka::Librd::Error::to_string($msg->err) } } );
        print STDERR "===== msg err =================================\n";
        next;
    }
    
    print STDERR Dumper( { key => $msg->key, partition => $msg->partition, offset => $msg->offset } );

    print STDERR Dumper $msg->payload;
    print STDERR "===== msg payload =============================\n";

    $kafka_consumer->commit_message($msg, 1);
}

print STDERR "===== unsubscribe =============================\n";
$kafka_consumer->unsubscribe;

sleep 10
