use strict;
use warnings;

use Data::Dumper;

use lib 'lib';
use Core::Ctx;

my $f = $ARGV[0];
die 'no file $ARGV[0]' unless $f;
die "file $f not found" unless -f $f;

my $ctx = Core::Ctx->new();

my $pn = $ctx->j_util->load_json_file($ctx, $f);
die 'input Petri Net seems weird' unless $pn;

my $dot = $ctx->render->render_graphviz_dot($ctx, $pn, 'test_' . time);
my $png = $ctx->render->graphviz_dot_to_png($ctx, $dot);
