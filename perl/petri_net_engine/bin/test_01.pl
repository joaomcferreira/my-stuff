use strict;
use warnings;

use Data::Dumper;

use lib 'lib';
use Core::Ctx;

my $f = $ARGV[0];
die 'no file $ARGV[0]' unless $f;
die "file $f not found" unless -f $f;

my $e = $ARGV[1];
die 'no event ($ARGV[1])' unless $e;

sleep 1;
my $now = time;
my $ctx = Core::Ctx->new();

my $in_pn = $ctx->j_util->load_json_file($ctx, $f);
die 'input Petri Net seems weird' unless $in_pn;

my $cp_pn = $ctx->j_util->copy_ref_data($ctx, $in_pn);
#print STDERR Dumper $cp_pn;

my $result = $ctx->pn_ngn->handle_single_event($ctx, $cp_pn, { name => $e });
print STDERR Dumper $result;
my $pn_out = $result->{pn};
$ctx->j_util->save_json_file($ctx, $pn_out, $f);

my $name = 'test_' . time;
my $dot = $ctx->render->render_graphviz_dot($ctx, $cp_pn, $name);
my $png = $ctx->render->graphviz_dot_to_png($ctx, $dot);
