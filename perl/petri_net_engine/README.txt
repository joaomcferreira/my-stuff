So many times a programmer is dealing with more or less complex synchronization of asynchronous processes, command and control, maintenance of not-so-easy state data and such. Petri Nets can act as an architectural foundation upon which such problems may be addressed in an organized and sistematic way. This project is an ilustration of such an approach.

The execution of the Petri Net engine is ilustrated by an unit test that defines a simple Petri Net and verifies its reaction to a small amount of events. Meawhile the engine is feeding back information that updates the caller with state change events in the Petri Net. At any time the overall Petri Net state must be preserved outside the engine.

https://en.wikipedia.org/wiki/Petri_net

Enjoy

Joao, 20201114

this looks cool: https://resources.jointjs.com/demos/pn
