package Util::DummyHandler;
use strict;
use warnings;

use Data::Dumper;

use Moo;
use experimental qw/signatures/;

###############################################################################
sub handle_token_change($self, $ctx, $pk, $n_initial, $n_final) {
   #$ctx->logger->log_debug("place $pk change from $n_initial to $n_final tokens");
};

###############################################################################
1;
