package Util::Logger;
use strict;
use warnings;

use Data::Dumper;

use Moo;
use experimental qw/signatures/;

sub _pre_format( $self, $x ) { $x =~ s/\n/ /g; $x =~ tr/  / /ds; return $x; }
sub _print_line( $self, $x ) { print STDERR '[' . time . '] ' . $self->_pre_format($x) . "\n"; }
sub generic_log( $self, $x ) { return $self->_print_line($x); }

sub log_info  ( $self, $x ) { $self->generic_log('_INFO: ' . $x); }
sub log_debug ( $self, $x ) { $self->generic_log('DEBUG: ' . $x); }
sub log_error ( $self, $x ) { $self->generic_log('ERROR: ' . $x); }
sub log_fatal ( $self, $x ) { $self->generic_log('FATAL: ' . $x); die 'dying because ' . $x; };

sub debug ( $self, @args ) { $self->log_debug('debug: ' . Dumper \@args); }
sub error ( $self, @args ) { $self->log_error('error: ' . Dumper \@args); }

1;
