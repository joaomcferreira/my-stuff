package Core::Render;
use strict;
use warnings;

use Data::Dumper;

use Moo;
use experimental qw/signatures/;

###############################################################################
sub graphviz_dot_to_png($self, $ctx, $dot) {
    my $dot_file = '/tmp/x.dot';
    my $png_file = '/tmp/x.png';
    system("echo '$dot' > $dot_file");
   #system("dot -Tpng $dot_file > $png_file");
    system("neato -n -Tpng $dot_file > $png_file");
    print STDERR "PNG file: $png_file\n";
    return $png_file;
};

###############################################################################
sub render_graphviz_dot($self, $ctx, $pn, $nm) {
    $nm ||= 'untitled';
    $pn ||= {        };

    my $dot = "digraph $nm { \n";

    $dot .= '    node [shape=circle];';
    foreach my $pk (keys %{ $pn->{places} || {} }) {
        my $pos = $pn->{places}->{$pk}->{pos};
        my $tks = $pn->{places}->{$pk}->{tokens} || 0;
        $dot .= " $pk".'[pos="'. $pos .'",label="' . "$pk\n$tks" . '"]'.';';
    };
    $dot .= "\n";

    $dot .= '    node [shape=box];';
    foreach my $tk (keys %{ $pn->{transitions} || {} }) {
        my $pos = $pn->{transitions}->{$tk}->{pos};
        $dot .= " $tk" . '[pos="'. $pos .'"]' . ';';
    };
    $dot .= "\n";

    foreach my $tk (keys %{ $pn->{transitions} || {} }) {
       #print STDERR $tk . "\n";
        my $t = $pn->{transitions}->{$tk};
        foreach my $pk ( @{ $t->{i_places} || [] } ) { $dot .= "    $pk" . '->' . $tk . ";\n"; };
        foreach my $pk ( @{ $t->{o_places} || [] } ) { $dot .= "    $tk" . '->' . $pk . ";\n"; };
    };

    $dot .= "    overlap=false\n";
    $dot .= '    label="' . $nm . '"' . "\n";
    $dot .= "    fontsize=10\n";
    $dot .= "}\n";
    return $dot;
};

###############################################################################
1;

###############################################################################
# strict graph {
#     node0 [pos="1,2"];
#     node1 [pos="2,3"];
# }
# 
###############################################################################
# digraph test_1605452621 { 
#     node [shape=box]; t_1; t_s; t_3; t_2;
#     node [shape=circle]; p_b; p_a; p_c;
#     p_a->t_1;
#     t_1->p_b;
#     t_s->p_a;
#     p_c->t_3;
#     t_3->p_a;
#     p_b->t_2;
#     t_2->p_c;
#     overlap=false
#     label="PetriNet"
#     fontsize=10
# }
# 
###############################################################################
# digraph TrafficLights {
# node [shape=box];  gy2; yr2; rg2; gy1; yr1; rg1;
# node [shape=circle,fixedsize=true,width=0.9];  green2; yellow2; red2; safe2; safe1; green1; yellow1; red1;
# gy2->yellow2;
# rg2->green2;
# yr2->safe1;
# yr2->red2;
# safe2->rg2;
# green2->gy2;
# yellow2->yr2;
# red2->rg2;
# rg1->green1;
# yr1->safe2;
# yr1->red1;
# safe1->rg1;
# green1->gy1;
# yellow1->yr1;
# red1->rg1;
# 
# overlap=false
# label="PetriNet (dot -Tpng test.dot > output.png)"
# fontsize=12;
# }
# 
###############################################################################
