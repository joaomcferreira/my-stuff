package Core::Ctx;
use strict;
use warnings;

use Util::Logger;
use Util::JSON;
use Core::Engine;
use Core::Render;

use Moo;
use experimental qw/signatures/;

my @ro_L1_B1_R0 = (is => 'ro', lazy => 1, builder => 1, required => 0);

has 'j_util' => @ro_L1_B1_R0;
has 'logger' => @ro_L1_B1_R0;
has 'pn_ngn' => @ro_L1_B1_R0;
has 'render' => @ro_L1_B1_R0;

sub _build_j_util ($self) { return Util::JSON  ->new(); }
sub _build_logger ($self) { return Util::Logger->new(); }
sub _build_pn_ngn ($self) { return Core::Engine->new(); }
sub _build_render ($self) { return Core::Render->new(); }

1;
