#!/usr/bin/perl
use strict;
use warnings;

sub get_2_arrays {
    my @to_add = ( 101, 102, 103 );
    my @to_rem = ( 901, 902, 903 );

    return wantarray ? (@to_add, @to_rem) : [ @to_add, @to_rem ];
};

