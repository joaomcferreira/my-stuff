my $text = 'First Line' . "\n" . '2nd Line' . "\n" . '3rd Line' . "\n";
print $text;
print "\n";

my ($p1, $p2, $p3, $p4) = split("\n", $text, 50);

print "---- parts ----\n";
print $p1 . "\n";
print $p2 . "\n";
print $p3 . "\n";
print $p4 . "\n";
print "---- parts ---.\n";

my $text2 = 'aaaaaaaaaaaa';
my ($z1, $z2) = split("\n", $text2, 40);
print "---- parts2 ----\n";
print $z1 . "\n";
print $z2 . "\n";
print "---- parts2 ---.\n";
