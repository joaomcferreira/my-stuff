# --
# Copyright (C) 2001-2022 OTRS AG, https://otrs.com/
# --
# This software comes with ABSOLUTELY NO WARRANTY. For details, see
# the enclosed file COPYING for license information (GPL). If you
# did not receive this file, see https://www.gnu.org/licenses/gpl-3.0.txt.
# --

package Kernel::System::Console::Command::Maint::Ticket::TitleSearch;

use strict;
use warnings;

use parent qw(Kernel::System::Console::BaseCommand);

our @ObjectDependencies = (
    'Kernel::System::Ticket',
);

=head1 NAME

Kernel::System::Console::Command::Maint::Ticket::TitleSearch
- search OTRS tickets by title.

=head1 DESCRIPTION

This package is an otrs.Console.pl module to allow searching OTRS
tickets by matching their title to some requested string given by
the mandatory '--title' option. There are no other options.

The search is performed by enriching the requested string with
wildcards at the beginning and at the end, in order to find as many
tickets as possible.

=head1 PUBLIC INTERFACE

=head2 Configure()

Configures the current instance of BaseCommand by specifying the
command description and the mandatory '--title' option.

=cut

sub Configure {
    my ( $Self, %Param ) = @_;

    $Self->Description('Performs title search.');
    $Self->AddOption(
        Name        => 'title',
        Description => 'The pattern to search for.',
        Required    => 1,
        HasValue    => 1,
        ValueRegex  => qr/^\w+$/smx,
    );

    return;
}

=head2 Run()

Executes the search and prints a short summary of the results. Uses
available methods from 'Kernel::System::Ticket' to perform the search
and to obtain Ticket data for the summary text line to print.

If there are no matching tickets an error message is printed in yellow
color and an exit error code is returned to the caller. Otherwise, the
the matching tickets summary text lines will is printed and a non-error
exit code is returned to the caller.

=cut

sub Run {
    my ( $Self, %Param ) = @_;

    my $TitleOption  = $Self->GetOption('title');
    my $TicketObject = $Kernel::OM->Get('Kernel::System::Ticket');

    #find matching tickets
    my @TicketIDs = $Self->_GetTicketIDsByTitle( $TicketObject, $TitleOption );
    my $TicketCount = scalar @TicketIDs;

    #warn user and error exit if there are no matching tickets
    unless ($TicketCount) {
        $Self->Print("<yellow>No tickets found, please use another search term!</yellow>\n");
        return $Self->ExitCodeError();
    };

    #display results
    $Self->Print("Results: $TicketCount\n--\n");
    $Self->_PrintTicketSummary( $TicketObject, $_ ) for @TicketIDs;

    return $Self->ExitCodeOk();
}

=head1 PRIVATE INTERFACE

=head2 _GetTicketIDsByTitle()

Returns a list of integers containing the database IDs of the Tickets
that match the requested title sub string.

    my @IDs = $Self->_GetTicketIDsByTitle(
        $Self           # reference; current running object
        $TicketObject   # reference; Ticket related methods object
        $TitleSubString # string; the text to match ticket titles
    );

=cut

sub _GetTicketIDsByTitle {
    my ( $Self, $TicketObject, $TitleSubString ) = @_;

    return $TicketObject->TicketSearch(
        Result              => 'ARRAY',
        UserID              => 1,
        Title               => '%' . $TitleSubString . '%',
        SortBy              => ['TicketNumber'],
        OrderBy             => ['Up'],
    );
}

=head2 _PrintTicketSummary()

Retrives Ticket data from database using the provided TicketID
and prints a short one line summary with Ticket details.

    $Self->_PrintTicketSummary(
        $Self         # reference; current running object
        $TicketObject # reference; Ticket related methods object
        $TicketID     # integer; Ticket ID to print a summary for
    );

=cut

sub _PrintTicketSummary {
    my ( $Self, $TicketObject, $TicketID ) = @_;

    my %Ticket = $TicketObject->TicketGet(
        TicketID => $TicketID,
        UserID   => 1,
    );

    $Self->Print("$Ticket{TicketNumber} - $Ticket{Title}\n");

    return;
}

1;

=head1 TERMS AND CONDITIONS

This software is part of the OTRS project (L<https://otrs.org/>).

This software comes with ABSOLUTELY NO WARRANTY. For details, see
the enclosed file COPYING for license information (GPL). If you
did not receive this file, see L<https://www.gnu.org/licenses/gpl-3.0.txt>.

=cut
