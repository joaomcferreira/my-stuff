#!/usr/bin/perl -w
#cpanm -v ExtUtils::Installed
use ExtUtils::Installed;

my $inst = ExtUtils::Installed->new();
my @ms = $inst->modules();

foreach $m (@ms){
    my $v = $inst->version($m) || '???';
    print $m . ' ' . $v . "\n";
}
