use strict;
use warnings;

use Data::Validate::UUID qw( is_uuid );

my $name = 'Joao';

if ( is_uuid( $name ) ) {
    print "my name is very wierd\n";
}
else {
    print "my name is NOT an UUID\n";
};

