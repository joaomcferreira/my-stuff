#
#Usage: "perl groceries.pl"
#
use strict;
use warnings;

use experimental qw/signatures/;

###############################################################################
sub print_count_and_sum( $list_of_integers, $format_string ) {
  my $data = { count => 0, sum => 0 };#avoid printing undefs

  foreach my $integer_value ( @{ $list_of_integers || [] } ) {
    $data->{count} += 1;
    $data->{sum} += $integer_value || 0;
  };

  $format_string =~ s!__SUM__!$data->{sum}!;
  $format_string =~ s!__COUNT__!$data->{count}!;
  print "$format_string\n";
}

###############################################################################
my $groceries = [
  { 'name' =>  'apples', 'baskets' =>   [10,20,30] },
  { 'name' => 'bananas', 'baskets' => [5,20,10,10] },
  { 'name' => 'oranges', 'baskets' =>       undef  },
  { 'name' =>   'kiwis', 'baskets' => [5,undef,undef,undef,undef,undef,undef,undef,undef,5] },
];

print_count_and_sum($_->{'baskets'}, "__SUM__ $_->{'name'} in __COUNT__ baskets") for @$groceries;

###############################################################################
# name is the name of a grocery product.
# baskets is a list that contains the quantity in each basket sold.
# Write a Perl script that displays for each product the total number of baskets and the total number of products. For example:
# 60 apples in 3 baskets
# 45 bananas in 4 baskets
#     The script need not accept any arguments; the above data can be hard-coded.
#     The script need not convert from JSON. You may choose to hard-code the data in a structure of your choice.
#     The script MUST calculate the results; it MUST NOT print hard-coded results.
###############################################################################
#--> What can make software hard to test?
#too big functions (in the sense of byte-count-size and also "function does to many things" sense)
#excessive assumption (inversion of control principles can help reducing assumption)
#functions reading or writing instance variables (functions are not static enough; maybe functions are tied to a given data model)
#excessive encapsulation (for example: embedding business logic into data-holding classes/packages)
###############################################################################
