use strict;
use warnings;
use Data::Dumper;

use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use MIME::Base64 ();
use Path::Tiny;

###################################################################
sub enc_b64 {
    return MIME::Base64::encode_base64( shift );
};

sub dec_b64 {
    return MIME::Base64::decode_base64( shift );
};

sub load_binary_file {
    my $full_path = shift;
    my $file = path($full_path);
    my $bytes = $file->slurp_raw;
    return $bytes;
};

sub save_binary_file {
    my $file = shift;
    my $bytes = shift;
    my $x = $file->spew_raw($bytes);
    print STDERR Dumper $x;
    return 1;
};

###################################################################
my $now = time;
my $ops_dir   = './data';
my $ark_root  = 'data';

my $temp1 = Path::Tiny->tempfile;
my $temp2 = Path::Tiny->tempfile;
my $zip_file1 = $temp1->stringify;
my $zip_file2 = $temp2->stringify;

###################################################################
my $zip = Archive::Zip->new();
$zip->addTree($ops_dir, $ark_root);

die "zip file write error $zip_file1" unless ($zip->writeToFileNamed($zip_file1) == AZ_OK);
die "zip file not created $zip_file1" unless (-f $zip_file1);

print $zip_file1 . "\n";

my $bytes = load_binary_file($zip_file1);
print "========== zip binary =================\n";
print $bytes;
print "\n";
print "========== zip binary =================\n";

my $b64 = enc_b64($bytes);

print "========== b64 ========================\n";
print $b64;
print "\n";
print "========== b64 ========================\n";

my $recovered_bytes = dec_b64($b64);

print "========== recovered zip binary =======\n";
print $recovered_bytes;
print "\n";
print "========== recovered zip binary =======\n";

print $zip_file2 . "\n";
my $foo = save_binary_file($temp2, $recovered_bytes);

sleep 60;
