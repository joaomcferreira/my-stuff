use strict;
use warnings;

use Data::Dumper;

my $path = $ENV{PATH};
my @keys = keys %ENV;
print STDERR Dumper %ENV;
print STDERR Dumper \@keys;

my $results = {};

foreach my $k (keys %ENV) {
    my $v = $ENV{$k};
    print STDERR 'chave|' . $k . '|valor|' . $v . "|\n";
    $results->{$k} = $v if ($k =~ /^X|^S/);
};

print STDERR Dumper $results;
