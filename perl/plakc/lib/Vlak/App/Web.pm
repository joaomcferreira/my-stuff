package Vlak::App::Web;
use strict;
use warnings;

use Moo;
use experimental qw/signatures/;

use Plack::Runner;

sub run ( $self, $psgi_app ) {

  my @server_options = (
   #'--disable-keepalive' => undef,#1
    '--server'            => 'Starman',
    '--port'              => 8228,
    '--host'              => '0.0.0.0',
    '--workers'           => 3,
    '--preload-app'       => 1,
  );

  my $runner = Plack::Runner->new;
  $runner->parse_options(@server_options);

  my $r = $runner->run($psgi_app);
  return $r;
}

1;
