package Vlak::Ctx;
use strict;
use warnings;

use Moo;
use experimental qw/signatures/;


###############################################################################
sub log_info( $self, $what ) {
    print STDERR "INFO $what\n";
    return 1;
}

sub log_fatal( $self, $what ) {
    die $what;
}

###############################################################################
use Vlak::Util::HTTP;
has 'h__util'  =>  ( is => 'ro', lazy => 1, builder => 1, required => 0 );
sub _build_h__util ( $self ) { return Vlak::Util::HTTP->new();          }

###############################################################################
1;
