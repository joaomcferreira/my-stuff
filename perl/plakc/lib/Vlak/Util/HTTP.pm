package Vlak::Util::HTTP;
use strict;
use warnings;

use Moo;
use experimental qw/signatures/;

use Plack::Response;

###############################################################################
sub plack_response_simple_ok ( $self, $msg ) {

    my $res = Plack::Response->new(200);
    $res->content_type('text/plain');
    $res->body('plack_response_simple_ok is very cool');

    my $f = $res->finalize;
   #print STDERR Dumper $f;
    return $f;
}
    
###############################################################################
sub http_response_error_pretty ( $self, $base_url, $status, $msg ) {
    my $text = "This is vlad-ws ($base_url $status $msg)";
    return [ $status , [ 'Content-type', 'text/plain' ], [ $text ] ];
};

sub http_response_ok ( $self, $msg, $mime_type ) {
    $mime_type ||= 'text/html';
    return [ 200 , [ 'Content-type', $mime_type ], [ $msg ] ];
};

sub http_response_ok_html ( $self, $msg) {
    return $self->http_response_ok ($msg, 'text/html')
};

###############################################################################
1;
