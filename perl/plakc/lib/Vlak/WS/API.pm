package Vlak::WS::API;
use strict;
use warnings;

use Module::Runtime qw/require_module/;
use Web::Simple;

use Moo;
use experimental qw/signatures/;

has 'ctx' => ( is => 'ro', required => 1 );

###############################################################################
my @base_routes_modules = ( 'Vlak::WS::API::Foo' );
my @more_routes_modules = ( 'Vlak::WS::API::Invalids' );

###############################################################################
my @all;

sub dispatch_request ( $self, $psgi_data ) {
    return @all;
};

sub BUILD {
    my @routes_instances = ();
    
    foreach my $m ( @base_routes_modules , @more_routes_modules ) {
      require_module $m;
      push @routes_instances, $m->new();
    }
  
    @all = map { $_->resources() } @routes_instances;
}

###############################################################################
1;

__END__
