package Vlak::WS::API::Foo;
use strict;
use warnings;

use Web::Simple;

###############################################################################
my $GET_foo = sub ( GET + /foo/* ) {
    my ( $self, $arg1, $psgi ) = @_;
    my $x = "you asked for foo $arg1 (PSGI $psgi->{HTTP_HOST} $psgi->{PATH_INFO} " . time . ')';
    $self->ctx->log_info($x);
    return $self->ctx->h__util->http_response_ok_html('<hr>' . $x . '<hr>');
};

my $GET_zoo = sub ( GET + /zoo/* ) {
    my ( $self, $arg1, $psgi ) = @_;
    my $x = "zoo $arg1 PSGI: $psgi->{HTTP_HOST} $psgi->{PATH_INFO}";
    return $self->ctx->h__util->plack_response_simple_ok($x);
};

###############################################################################
my $drop_dead = sub ( GET + /die/* ) {
    my ( $self, $arg1, $psgi ) = @_;
    $self->ctx->log_fatal('dying on request: ' . $arg1);
};

###############################################################################
sub resources() {
    return (
        $GET_foo,
        $GET_zoo,
        $drop_dead,
    );
}

###############################################################################
1;
