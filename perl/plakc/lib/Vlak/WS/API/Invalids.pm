package Vlak::WS::API::Invalids;
use strict;
use warnings;

use Web::Simple;

###############################################################################
my $xoneGET = sub ( GET ) {
    my $self = shift;
    return $self->ctx->h__util->http_response_error_pretty('not cool', 404, 'xoneGET');
};

my $xone405 = sub () {
    my $self = shift;
    return $self->ctx->h__util->http_response_error_pretty('not cool', 405, 'xone405');
};

###############################################################################
sub resources() {
    return (
        $xoneGET,
        $xone405,
    );
};

###############################################################################
1;
