_This_is:

A not so simple base structure for a Web API project using perl's Plack HTTP framework.

_Usage:

run: 'perl bin/vlak_api.pl' (should start the HTTP server)
check: 'curl localhost:8228/foo/123' (should produce some basic HTML response)
