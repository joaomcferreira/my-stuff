use strict;
use warnings;

use lib 'lib';

print "starting\n";

#
#API routes and application ctx
use Vlak::Ctx;
use Vlak::WS::API;
my $ctx = Vlak::Ctx->new();
my $web_simple_api = Vlak::WS::API->new(ctx => $ctx);
my $psgi_app = $web_simple_api->to_psgi_app;

#
#HTTP server && run it
use Vlak::App::Web;
my $plack_starman = Vlak::App::Web->new();
$plack_starman->run($psgi_app);

print "finishing\n";
