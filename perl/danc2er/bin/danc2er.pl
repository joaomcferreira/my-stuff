use strict;
use warnings;

use lib 'lib';

use Astaire::WS;
use Astaire::Ctx;

my $ctx = Astaire::Ctx->new();
my $fred = Astaire::WS->new();

$fred->set_context($ctx);
$fred->dance();
