package Util::Logger;
use strict;
use warnings;

use Moo;
use experimental qw/signatures/;

###############################################################################
sub log_info( $self, $what ) {
    my $text = "INFO $what";
    print STDERR "$text\n";
    return $text;
}

sub log_fatal( $self, $what ) {
    die "FATAL: $what";
}

###############################################################################
1;
