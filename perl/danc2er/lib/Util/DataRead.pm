package Util::DataRead;
use strict;
use warnings;

use Moo;
use experimental qw/signatures/;

sub get_musician($self, $style) {
    return 'Glenn Miller' if 'swing' eq $style;
    return 'Chuck Berry' if 'rock' eq $style;
    return undef;
}

1;
