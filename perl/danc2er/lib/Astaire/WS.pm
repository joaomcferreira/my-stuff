package Astaire::WS;
use strict;
use warnings;

use Dancer;

use Moo;
use experimental qw/signatures/;

###############################################################################
my $ctx;
sub set_context ($self, $context) {
    $ctx = $context;
};

###############################################################################
get '/hello' => sub {
    return 'Hello World!'
};

get '/rock' => sub {
    $ctx->logger->log_info('dancing and logging');
    my $musician = $ctx->dataread->get_musician('rock');
    return $musician;
};

get '/jazz' => sub {
    $ctx->logger->log_info('dancing and logging');
    my $musician = $ctx->dataread->get_musician('swing');
    return $musician;
};

get '/die' => sub {
    die 'bye, bye...';
};

###############################################################################
1;
