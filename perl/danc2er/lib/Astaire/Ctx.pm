package Astaire::Ctx;
use strict;
use warnings;

use Util::Logger;
use Util::DataRead;

use Moo;

has 'logger'    =>  ( is => 'ro', lazy => 1, builder => 1, required => 0 );
has 'dataread'  =>  ( is => 'ro', lazy => 1, builder => 1, required => 0 );

sub _build_logger   { return Util::Logger->new();    }
sub _build_dataread { return Util::DataRead->new();  }

1;
