_This_is:

A not so simple base structure for a Web API project using perl's Dancer2 HTTP framework.

_PreReqs

apt install libmoo-perl
apt install libdancer-perl

_Usage:

run: 'perl bin/danc2er.pl' (should start the HTTP server)
check: 'curl localhost:3000/rock' (should produce some basic response)
