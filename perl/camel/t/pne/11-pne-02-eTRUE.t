use strict;
use warnings;
use Data::Dumper;
use Test::More tests => 6;

use lib 'lib';
use Camel::App::Ctx;

my $ctx = Camel::App::Ctx->new();

my $pn = {
    places => {
        p_a => { tokens => 1 },
        p_b => { tokens => 0 },
        p_c => { tokens => 0 },
    },
    transitions => {
        t_1 => { name => 't_1', event => 'evt_1', i_places => ['p_a'], o_places => ['p_b'] },
        t_2 => { name => 't_2', event =>  'TRUE', i_places => ['p_b'], o_places => ['p_c']  },
        t_3 => { name => 't_3', event => 'evt_3', i_places => ['p_c'], o_places => ['p_a']  },
    },
};

my $a = $ctx->pn_ngn->handle_event($ctx, $pn, { name => 'evt_1' });
is($pn->{places}->{p_a}->{tokens}, 0, 'check correct Petri Net execution a1');
is($pn->{places}->{p_b}->{tokens}, 0, 'check correct Petri Net execution a2');
is($pn->{places}->{p_c}->{tokens}, 1, 'check correct Petri Net execution a3');

#print STDERR Dumper $a;
is($a->{changes}->[3]->{place}        , 'p_c', 'check correct reporting of place token changes a');
is($a->{changes}->[3]->{tokens_before},    0 , 'check correct reporting of place token changes b');
is($a->{changes}->[3]->{tokens_after} ,    1 , 'check correct reporting of place token changes c');

$ctx->tdummy->handle_token_change($ctx, $_->{place}, $_->{tokens_before}, $_->{tokens_after}) for @{ $a->{changes} };
$ctx->pn_ngn->print_pn($pn, 'test_pn');

done_testing();
