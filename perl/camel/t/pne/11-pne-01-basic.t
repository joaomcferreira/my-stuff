use strict;
use warnings;
use Data::Dumper;
use Test::More;

use lib 'lib';
use Camel::App::Ctx;

my $ctx = Camel::App::Ctx->new();

my $pn = {
    places => {
        p_a => { tokens => 1 },
        p_b => { tokens => 2 },
        p_c => { tokens => 0 },
    },
    transitions => {
        t_1 => { name => 't_1', event => 'evt_1', i_places => ['p_a'], o_places => ['p_b'] },
        t_2 => { name => 't_2', event => 'evt_2', i_places => ['p_b'], o_places => ['p_c']  },
        t_3 => { name => 't_3', event => 'evt_3', i_places => ['p_c'], o_places => ['p_a']  },
    },
};

my $a = $ctx->pn_ngn->handle_event($ctx, $pn, { name => 'evt_2' });
is($pn->{places}->{p_a}->{tokens}, 1, 'check correct Petri Net execution a1');
is($pn->{places}->{p_b}->{tokens}, 1, 'check correct Petri Net execution a2');
is($pn->{places}->{p_c}->{tokens}, 1, 'check correct Petri Net execution a3');

$ctx->pn_ngn->handle_event($ctx, $pn, { name =>     'x' });
$ctx->pn_ngn->handle_event($ctx, $pn, { name => 'evt_3' });
is($pn->{places}->{p_a}->{tokens}, 2, 'check correct Petri Net execution b1');
is($pn->{places}->{p_b}->{tokens}, 1, 'check correct Petri Net execution b2');
is($pn->{places}->{p_c}->{tokens}, 0, 'check correct Petri Net execution b3');

done_testing();
