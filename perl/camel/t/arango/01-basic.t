#!/usr/bin/env perl
use Data::Dumper;

use Test::Most tests => 16;

use lib 'lib';

use Camel::App::Ctx;
use Camel::Util::ArangoDB;
use Camel::Util::ArangoSchema;

use_ok('Camel::App::Ctx');
use_ok('Camel::Util::ArangoDB');
use_ok('Camel::Util::ArangoSchema');

my $ctx = Camel::App::Ctx->new();
my $au  = Camel::Util::ArangoDB->new(logger => $ctx->logger);
my $as  = Camel::Util::ArangoSchema->new(logger => $ctx->logger);

my $a_cfg = { host => 'localhost', port => 8529, username => 'root', password => 'root' };
print STDERR Dumper $a_cfg;
my $a = $au->get_arango_handle($a_cfg);

my $dont_die  = undef;
my $max_tries = 3;
my $nap_delay = 1;
my $wait = $au->wait_for_arango($ctx, $a, $dont_die, $max_tries, $nap_delay);
is ($wait, 'ok', 'db connection ok for root user');
die 'can not proceed because database is not up' unless $wait eq 'ok';

my ($schema, $db_name, $version) = $as->parse('t/arango/schema.json');
is($db_name, 'test_db', 'arango schema parse ok');

$as->instantiate($ctx, $a, $db_name, $schema, $au);
is ($au->database_exists($ctx, $a, $db_name), 1, 'database exists after instantiate');

$au->database_delete_safe($ctx, $a, $db_name);
is ($au->database_exists($ctx, $a, $db_name), 0, 'database does not exist after delete');

$as->instantiate($ctx, $a, $db_name, $schema, $au);
is ($au->database_exists($ctx, $a, $db_name), 1, 'database exists after instantiate again');

$as->instantiate($ctx, $a, $db_name, $schema, $au);
is ($au->database_exists($ctx, $a, $db_name), 1, 'database exists after re-instantiate');

$as->instantiate($ctx, $a, $db_name, $schema, $au);
is ($au->database_exists($ctx, $a, $db_name), 1, 'database exists after re-instantiate again');

my $db = $au->database_get($ctx, $a, $db_name);

my @check_collections = (
    { n => 'NotTbFound'      , x => 0 },
    { n => 'TestCollectionA' , x => 1 },
);

foreach my $t ( @check_collections ) {
    my $text = $t->{x} ? ' exists' : ' does not exist';
    is ($au->collection_exists($ctx, $db, $t->{n}), $t->{x}, 'check that collection ' . $t->{n} . $text);
};

my $idxs = $au->get_all_indexes($ctx, $db, 'TestCollectionA');
is (scalar @{ $idxs->{indexes} } ,        2 , 'indexes created ok (number of indexes)');
is ($idxs->{indexes}->[0]->{name}, 'primary', 'indexes created ok (primary index)'    );
is ($idxs->{indexes}->[1]->{name},   'ttl_A', 'indexes created ok (test ttl index)'   );

$au->database_delete_safe($ctx, $a, $db_name);
is ($au->database_exists($ctx, $a, $db_name), 0, 'database does not exist after delete again');

done_testing();
