package Mock::Ctx;
use strict;
use warnings;

use Moo;

use Camel::App::Ctx;
use Mock::UA;

extends 'Camel::App::Ctx';

sub _build_ua() { return Mock::UA->new(); }

1;
