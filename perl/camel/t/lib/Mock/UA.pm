package Mock::UA;
use strict;
use warnings;

use Moo;
use experimental qw/signatures/;

sub request($self, $arg1, $arg2, $arg3){
    print STDERR __PACKAGE__ . "\n";

    if ($arg2 eq 'http://test/issue/ABC-123') {
        return {
            success => 1,
            content => '{"fields":{"a":"b"}}'
        }
    };
    
    return undef;
}

1;
