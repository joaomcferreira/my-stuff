package TestRunner;
use strict;
use warnings;

use Camel::App::_abstractRunner;

use Moo;
use experimental qw/signatures/;

extends 'Camel::App::_abstractRunner';

sub run($self, $name){
    print STDERR "TestRunner->run() $name; seems ok.\n";
    return $name;
}

1;
