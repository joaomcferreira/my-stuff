use strict;
use warnings;
use Data::Dumper;
use Test::Most tests => 2;

use lib 'lib';
use Camel::Util::Jira;

use lib 't/lib';
use Mock::Ctx;

my $ctx = Mock::Ctx->new();
my $jira = Camel::Util::Jira->new();

my $r1 = $jira->get_issue_details($ctx, 'ABC-123', 'http://test', 'Smith', 'a1b2c3d4e5');
my $r2 = $jira->get_issue_details($ctx, 'ABC-321', 'http://test', 'Smith', 'a1b2c3d4e5');

is($r1->{fields}->{a},   'b', 'basic check Camel::Util::Jira (a)');
is($r2->{fields}->{a}, undef, 'basic check Camel::Util::Jira (b)');

done_testing();
