#!/usr/bin/env perl
use warnings;
use strict;

use lib 'lib';

use Test::Most;

my @modules = qw(
    Camel::App::Ctx
    Camel::Util::ArangoDB
    Camel::Util::ArangoSchema
    Camel::Util::AWSJargonParser
    Camel::Util::GitLab
    Camel::Util::Jira
    Camel::Util::Logger
    Camel::Util::SayHello
);

require_ok($_) for ( @modules );

done_testing();
