use strict;
use warnings;

use Test::Most tests => 5;

use lib './lib';

use Camel::Util::SayHello;

require_ok 'Camel::Util::SayHello';

my $greetings = Camel::Util::SayHello->new();

my $greeting_a = Camel::Util::SayHello::say_good_morning(undef, 'Sherlok');
my $greeting_b = Camel::Util::SayHello::say_hi(undef, 'Holmes');
my $greeting_c = $greetings->say_good_morning('Miss Daisy');
my $greeting_d = $greetings->say_hi('Donald');

is ($greeting_a,    'Good Morning, Sherlok :) Have a nice day!', '1st greeting is correct');
is ($greeting_b,                     "Hi, Holmes :) What's up?", '2nd greeting is correct');
is ($greeting_c, 'Good Morning, Miss Daisy :) Have a nice day!', '3rd greeting is correct');
is ($greeting_d,                     "Hi, Donald :) What's up?", '4th greeting is correct');

done_testing();
