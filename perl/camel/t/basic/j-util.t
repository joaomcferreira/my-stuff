use strict;
use warnings;

use Data::Dumper;
use Test::More tests => 3;

use lib 'lib';
use Camel::App::Ctx;

my $ctx = Camel::App::Ctx->new();

my $data = { foo => { bar => 'whatever' } };
my $copy = $ctx->j_util->copy_ref_data($ctx, $data);

isnt($copy, $data, 'check refs are not the same');
is($copy->{foo}->{bar}, 'whatever', 'basic check data correctly copied');

my $j_file = '/tmp/camel_json_test.json';
system("rm -rf $j_file");
die 'erased file still exists' if -f $j_file;

$ctx->j_util->save_json_file($ctx, $data, $j_file);
my $load = $ctx->j_util->load_json_file($ctx, $j_file);

is($load->{foo}->{bar}, 'whatever', 'basic check data correctly saved and loaded');
