use strict;
use warnings;

use Try::Tiny;
use Data::Dumper;
use Test::More tests => 4;

use lib 'lib';
use Camel::App::Ctx;

my $ctx = Camel::App::Ctx->new();

my $a = "   some  \n text";
my $d = $ctx->logger->log_debug($a);
my $e = $ctx->logger->log_error($a);
my $i = $ctx->logger->log_info ($a);
my $f = try {
    return $ctx->logger->log_fatal('x');
} catch {
    return 'caught something';
};

is($d, 'DEBUG: some text', 'basic check logger logs the right debug text');
is($e, 'ERROR: some text', 'basic check logger logs the right error text');
is($i, '_INFO: some text', 'basic check logger logs the right  info text');
is($f, 'caught something', 'basic check logger dies with log_fatal');
