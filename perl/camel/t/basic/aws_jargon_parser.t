use strict;
use warnings;

use Test::More tests => 5;

use lib 'lib';

use Camel::Util::AWSJargonParser;

my $p = Camel::Util::AWSJargonParser->new();

my ($a, $b, $c, $d, $e) = $p->parse_clusterArn('arn:aws:ecs:eu-west-3:123456789012:cluster/components-1');

is ($a,          'ecs', 'check parse_clusterArn ok 1');
is ($b,    'eu-west-3', 'check parse_clusterArn ok 2');
is ($c, '123456789012', 'check parse_clusterArn ok 3');
is ($d, 'components-1', 'check parse_clusterArn ok 4');
is ($e,         undef , 'check parse_clusterArn ok 5');

#TODO: test all other methods

done_testing();
