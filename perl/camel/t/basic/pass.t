use strict;
use warnings;

#------------------------ ------ ------ ------ ------ ------
#File                stmt   bran   cond    sub   time  total
#------------------------ ------ ------ ------ ------ ------
#t/contrib/pass.t    93.3   50.0   33.3  100.0   49.4   82.6
#------------------------ ------ ------ ------ ------ ------

use experimental qw/signatures/;

use Test::Most tests => 2;

sub does_nothing ( $condition1 , $condition2 ) {
  if ( $condition1 || $condition2 ) {#33% cond cov
    print "pass.t branch 1\n";
  }
  else {
    print "pass.t branch 2\n";#50% bran cov
  }
};

is(1, 1, 'always pass');
does_nothing(1 , 0);#notice: 93.3% stmt cov,50% bran cov and 33% cond cov
is(1, 1, 'always pass');

done_testing();
