use strict;
use warnings;

use Test::More tests => 1;

use lib 'lib';
use Camel::App::_abstractRunner;

use lib 't/lib';
use TestRunner;

my $app = TestRunner->new();

my $name = 'testing123x';
my $x = $app->please_run($name);

is ($x, $name, 'Camel::App::_abstractRunner');

done_testing();
