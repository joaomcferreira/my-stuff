package Camel::App::_abstractRunner;
use strict;
use warnings;

use Moo;
use experimental qw/signatures/;

sub setup_TERM_handler ($self) {
    my $msg = 'I am _abstractRunner->setup_TERM_handler. You can implement me.';
    $SIG{TERM} = sub { print STDERR $msg . "\n"; exit(0); };
    return 1;
}

sub run ($self, $name) {
    die "I am _abstractRunner->run. I will _NOT_ start $name. I am abstract. You must implement me.";
}

sub please_run ($self, $name) {
    print STDERR "_abstractRunner: $name start\n";
    my $t = $self->setup_TERM_handler();
    my $r = $self->run($name);
    print STDERR "_abstractRunner: $name stop\n";
    return $r;
}

1;
