package Camel::App::Ctx;
use strict;
use warnings;

use HTTP::Tiny;

use Camel::Util::Logger;
use Camel::Util::JSON;
use Camel::Util::PetriNetEngine;
use Camel::Util::TestDummy;

use Moo;
use experimental qw/signatures/;

my @ro_L1_B1_R0 = (is => 'ro', lazy => 1, builder => 1, required => 0);

has 'j_util' => @ro_L1_B1_R0;
has 'logger' => @ro_L1_B1_R0;
has 'pn_ngn' => @ro_L1_B1_R0;
has 'tdummy' => @ro_L1_B1_R0;
has 'ua'     => @ro_L1_B1_R0;

sub _build_j_util ($self) { return Camel::Util::JSON          ->new(); }
sub _build_logger ($self) { return Camel::Util::Logger        ->new(); }
sub _build_pn_ngn ($self) { return Camel::Util::PetriNetEngine->new(); }
sub _build_tdummy ($self) { return Camel::Util::TestDummy     ->new(); }
sub _build_ua     ($self) { return HTTP::Tiny                 ->new(); }

1;
