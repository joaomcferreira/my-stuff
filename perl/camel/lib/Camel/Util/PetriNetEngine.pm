package Camel::Util::PetriNetEngine;
use strict;
use warnings;

use Data::Dumper;

use Moo;
use experimental qw/signatures/;

###############################################################################
sub print_pn($self, $pn, $label) {
    $label ||= 'My Petri Pretty Net';
    print STDERR "=================================\n";
    print STDERR "$label\n";
    print STDERR "=================================\n";
    print STDERR Dumper $pn;
    print STDERR "=================================\n";
};

###############################################################################
sub t_is_receptive_to_e($self, $ctx, $tk, $pn, $e) {
    my $r = $e->{name} eq $pn->{transitions}->{$tk}->{event} ? 1 : 0;
    my $t_name = $pn->{transitions}->{$tk}->{name};
    my $y = $r ? '' : ' not';
    $ctx->logger->log_debug("transition $t_name is$y receptive to event " . $e->{name});
    return $r;
};

###############################################################################
sub t_in_places_are_all_active($self, $ctx, $tk, $pn) {
    my $ret_val = 1;

    my $ips = $pn->{transitions}->{$tk}->{i_places} || [];

    foreach my $pk ( @$ips ) {
        my $p = $pn->{places}->{$pk};
        die "place $pk does not exists" unless $p;
        $ret_val = 0 unless $p->{tokens};
        last unless $ret_val;
    };

    my $t_name = $pn->{transitions}->{$tk}->{name};
    my $y = $ret_val ? '' : ' not';
    $ctx->logger->log_debug("transition $t_name in_places are$y all active");
    return $ret_val;
};

###############################################################################
sub update_place_tokens($self, $ctx, $pk, $n, $pn) {
    my $p = $pn->{places}->{$pk};
    die "place $pk does not exist" unless $p;
    my $n_initial = $p->{tokens} || 0;
    my $n_final = $n_initial + $n;
    $p->{tokens} = $n_final;
    
    my $r = {
        place         => $pk,
        tokens_after  => $n_final,
        tokens_before => $n_initial,
    };

    $ctx->logger->log_debug("place $pk tokens changed from $n_initial to $n_final");

    return $r;
};

###############################################################################
sub fire_transition($self, $ctx, $tk, $pn) {
    my $ips = $pn->{transitions}->{$tk}->{i_places} || [];
    my $ops = $pn->{transitions}->{$tk}->{o_places} || [];

    my $x = [];#contains place token change entries
    push @$x, $self->update_place_tokens($ctx, $_, -1, $pn) for @$ips;
    push @$x, $self->update_place_tokens($ctx, $_, +1, $pn) for @$ops;

    return $x;
};

###############################################################################
sub handle_single_event($self, $ctx, $pn, $e) {

    my $token_changes = [];
    my $something_fired = 0;
    
    foreach my $tk (sort (keys %{ $pn->{transitions} || {} } ) ) {
        my $receptive   = $self->t_is_receptive_to_e($ctx, $tk, $pn, $e);
        my $shall_fire  = $self->t_in_places_are_all_active($ctx, $tk, $pn) if $receptive;
        my $fire_result = $self->fire_transition($ctx, $tk, $pn) if $shall_fire;
        $fire_result ||= [];
        push @$token_changes, @$fire_result;
        $something_fired = 1 if $shall_fire;
    };
    
    return {
        pn => $pn,
        changes => $token_changes,
        something_fired => $something_fired,
    };
};

###############################################################################
sub handle_event($self, $ctx, $pn, $e) {
    my $r = $self->handle_single_event($ctx, $pn, $e);
    
    my $max = 5;
    my $something_fired = $r->{something_fired};
    while ($something_fired) {
        $max--;
        die 'your Petri Net wont stabilize' unless $max; 
        my $rT = $self->handle_single_event($ctx, $pn, { name => 'TRUE' });
        $something_fired = $rT->{something_fired};
        push @{ $r->{changes} }, @{ $rT->{changes} }; 
    };
    
    return {
        pn => $pn,
        changes => $r->{changes}
    };
};

###############################################################################
1;
