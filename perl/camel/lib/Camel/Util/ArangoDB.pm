package Camel::Util::ArangoDB;
use strict;
use warnings;

use Arango::Tango;

use Moo;
use experimental qw/signatures/;

####################################### database ##############################
sub database_get( $self, $ctx, $a, $name) {
    my $db = $a->database($name);
    return $db;
}

sub database_create ( $self, $ctx, $a, $name ) {
    my $db = $a->create_database($name);
    return $db;
}

sub database_delete ( $self, $ctx, $a, $name ) {
    my $x = $a->delete_database($name);
    return $x;
}

sub database_exists ( $self, $ctx, $a, $name ) {
    my $visible_names = $a->list_databases();
    my $c = scalar @{ $visible_names || [] };
   
    my $exists = 0;
    $exists = 1 if grep { /^$name$/ } @$visible_names;
    
    my $text = $exists ? 'exists' : 'does not exist (?)';
    
    $ctx->logger->log_debug('found ' . $c . ' databases');
    $ctx->logger->log_debug('database ' . $name . ' ' . $text);
    return $exists;
}

sub database_create_safe ( $self, $ctx, $a, $name ) {
    $ctx->logger->log_debug('create database ' . $name);
    my $exists = $self->database_exists($ctx, $a, $name);
    return $self->database_get($ctx, $a, $name) if $exists;
    return $self->database_create($ctx, $a, $name);
}

sub database_delete_safe ( $self, $ctx, $a, $name ) {
    $ctx->logger->log_info('database_delete _______________________[begin]');
    $ctx->logger->log_debug('delete database ' . $name);
    
    my $result = 0;    
    if ( $self->database_exists($ctx, $a, $name ) ) {
        my $x = $self->database_delete($ctx, $a, $name );
        #print STDERR Dumper $x;
        if ('200' eq $x->{code}) {
            $ctx->logger->log_info('delete_database returns 200. seems ok.');
            $result = 1;
        }
        else {
            $ctx->logger->log_error('delete_database does not return 200. this is weird. BE CAREFUL.');
        };
        
        $self->database_exists($ctx, $a, $name );
    }
    else {
        $ctx->logger->log_error('database does not exist. nothing to delete.');
    };
    
    
    $ctx->logger->log_info('database_delete _________________________[end]');
    return $result;
}

####################################### user ##################################
sub user_create( $self, $ctx, $a, $name, $permission) {
    my $u = $a->create_user($name);
    $ctx->logger->log_debug('user ' . $u->{user} . ' created');
    return $u;
}

sub user_get( $self, $ctx, $a, $name) {
    my $u = $a->user($name);
    return $u;
}

sub user_exists( $self, $ctx, $a, $name) {
    my $visible_users = $a->list_users();
    my $safe = $visible_users->{result} || [];
    my $count = scalar @{ $safe };
   
    my $exists = 0;
    foreach my $c ( @{ $safe } ) {
        if ($name eq $c->{user}) {
            $exists = 1;
            last;
        };
    };
    
    my $text = $exists ? 'exists' : 'does not exist';
    $ctx->logger->log_debug('found ' . $count . ' users');
    $ctx->logger->log_debug('user ' . $name . ' ' . $text);
    return $exists;
}

sub user_ensure_exists( $self, $ctx, $a, $name, $permission) {
    unless ($self->user_exists($ctx, $a, $name)) {
        $ctx->logger->log_debug('user ' . $name . ' does not exist');
        $self->user_create($ctx, $a, $name, $permission);
        $ctx->logger->log_fatal('user ' . $name . ' still does not exist after so much effort') unless $self->user_exists($ctx, $a, $name);
    };
    
    my $u = $self->user_get($ctx, $a, $name);
    return $u;
}

####################################### collection ############################
sub collection_create ( $self, $ctx, $db, $name, $opts) {
    my $c = $db->create_collection($name, %$opts);
    return $c;
}

sub collection_get( $self, $ctx, $db, $name) {
    my $c = $db->collection($name);
    return $c;
}

sub collection_exists( $self, $ctx, $db, $name) {
    my $visible_collections = $db->list_collections();
    my $safe = $visible_collections || [];
    my $count = scalar @{ $safe };

    my $exists = 0;
    foreach my $c ( @{ $safe } ) {
        if ($name eq $c->{name}) {
            $exists = 1;
            last;
        };
    };

    my $text = $exists ? 'exists' : 'does not exist';
    $ctx->logger->log_debug('found ' . $count . ' collections');
    $ctx->logger->log_debug('collection ' . $name . ' ' . $text);
    return $exists;
}

sub collection_create_safe ( $self, $ctx, $db, $name, $t) {
    my $opts;
    $opts->{type} = '2';
    $opts->{type} = '3' if $t eq 'edge';
    $opts->{type} =  $t if $t =~ /^\d$/;
    
    $self->collection_create($ctx, $db, $name, $opts) unless $self->collection_exists($ctx, $db, $name);

    $ctx->logger->log_fatal('collection ' . $name . ' still does not exist. this is a problem.')
        unless $self->collection_exists($ctx, $db, $name );

    my $c = $self->collection_get($ctx, $db, $name);
    return $c;
}

####################################### indexes ###############################
sub ttl_index_create ( $self, $ctx, $db, $colname, $opts) {
    my $i = $db->create_ttl_index( $colname, %$opts);
    return $i;
}

sub get_all_indexes ( $self, $ctx, $db, $colname) {
    my $i = $db->get_indexes($colname);
    return $i;
}

####################################### connection ############################
sub db_connection_ok ( $self, $ctx, $a) {
    return $self->database_exists($ctx, $a, '_system');;
}

sub wait_for_arango ( $self, $ctx, $a, $dont_die, $max, $nap) {
    $max ||= 10;
    $nap ||=  1;

    my $c = 0;
    while($c < $max) {
        $c++;

        my $is_ok;
        if ($dont_die) {
            $is_ok = eval { return $self->db_connection_ok($ctx, $a); };
        }
        else {
            $is_ok = $self->db_connection_ok($ctx, $a);
        };
        
        if ($is_ok) {
            $ctx->logger->log_info('ArangoDB is up');
            last;
        }

        $ctx->logger->log_info("Waiting for ArangoDB to come online $c/$max");
        sleep $nap;
    };
  
    return 'ok' if $c < $max;
    
    my $what = 'Failed to connect to ArangoDB';
    $ctx->logger->log_fatal($what) unless $dont_die;
    $ctx->logger->log_error($what);
    return;
}

sub get_arango_handle ( $self, $config ) {
    my $a = Arango::Tango->new( %{ $config } );
    return $a;
}

sub get_db_handle ( $self,  $config, $db_name ) {
    my $a   = $self->get_arango_handle($config);
    my $dbh = $self->database_get($a, $db_name);
    return $dbh;
}

###############################################################################

1;
