package Camel::Util::SayHello;
use strict;
use warnings;

use Moo;
use experimental qw/signatures/;

sub say_hi ( $self, $john_doe ) {
    my $what_to_say = "Hi, $john_doe :) What's up?";
    print STDERR $what_to_say . "\n";
    return $what_to_say;
}

sub say_good_morning ( $self, $jane_doe ) {
    my $what_to_say = "Good Morning, $jane_doe :) Have a nice day!";
    print STDERR $what_to_say . "\n";
    return $what_to_say;
}

1;
