package Camel::Util::ArangoSchema;
use strict;
use warnings;

use File::Slurp qw/read_file/;
use JSON::XS;

use Moo;
use experimental qw/signatures/;

sub _load_json( $self, $file) {
    my $text = read_file($file);#expected to be JSON
    my $data = JSON::XS->new->allow_nonref(1)->decode($text);
    return $data;
}

sub parse( $self, $schema_file) {
    my $schema = $self->_load_json($schema_file);
    my $version = $schema->{schema_version};
    my $db_name = $schema->{database_name};

    unless ($db_name && $version) {
        $self->logger->log_error('could read schema db_name or version');
        return;
    };
    
    return ($schema, $db_name, $version);
}

sub instantiate( $self, $ctx, $a, $db_name, $schema, $au) {
    $ctx->logger->log_info('instantiate_schema_________________[begin]');

    $ctx->logger->log_info('create users');
    foreach my $u_name (sort (keys ( %{ $schema->{database_users} } ) ) ) {
        my $p = $schema->{database_users}->{$u_name}->{permission};
        my $u = $au->user_ensure_exists($ctx, $a, $u_name, $p);
    };

    $ctx->logger->log_info('create database '. $db_name);
    my $db = $au->database_create_safe($ctx, $a, $db_name);
    $ctx->logger->log_fatal ('database is undefined. this is a problem.') unless $db;
    $ctx->logger->log_fatal ('database doesnt exist. this is a problem.') unless $au->database_exists($ctx, $a, $db_name);

    $ctx->logger->log_info('create collections');
    foreach my $c_name (sort (keys ( %{ $schema->{database_schema}->{collections} } ) ) ) {
        my $t = $schema->{database_schema}->{collections}->{$c_name}->{type};
        my $c = $au->collection_create_safe($ctx, $db, $c_name, $t);
        $ctx->logger->log_error('collection ' . $c_name . ' is inconsistent. this is a problem.') unless ($c_name eq $c->info->{name});
    };

    $ctx->logger->log_info('create indexes');
    foreach my $i_name (sort (keys ( %{ $schema->{database_schema}->{indexes} || {} } ) ) ) {
        my $i = $schema->{database_schema}->{indexes}->{$i_name};
        my $d = $i->{data};
        my $t = $d->{type};
        
        unless ($t eq 'ttl') {
            $ctx->logger->log_error('index type not supported: ' . $i_name . '; type: ' . $t . ';');
            next;
        };

        my $c = $i->{collection};
        $d->{name} = $i_name unless $d->{name};
        
        $ctx->logger->log_info('create index: ' . $i_name . '; type: ' . $t . ';');
        $au->ttl_index_create($ctx, $db, $c, $d);
    };

    $ctx->logger->log_info('instantiate_schema___________________[end]');
    return 1;
}

1;
