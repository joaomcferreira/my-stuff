package Camel::Util::AWSJargonParser;
use strict;
use warnings;

use Moo;
use experimental qw/signatures/;

#'taskDefinitionArn' => 'arn:aws:ecs:eu-west-3:123456789012:task-definition/foo-worker:183',
sub parse_taskDefinitionArn($self, $x) {
    my ($f, $z, $a, $c, $v);
    
    if ($x =~ m|^arn:aws:(\S+):(\S+):(\d+):task-definition/(\S+):(\d+)$|) {
        $f = $1;#ecs, others???
        $z = $2;#zone
        $a = $3;#account
        $c = $4;
        $v = $5;#task definition version
    };
    
    return ($f, $z, $a, $c, $v);
};

#'arn:aws:ecs:eu-west-3:123456789012:service/whatever-this-that'
sub parse_serviceArn($self, $x) {
    my ($f, $z, $a, $c);
    
    if ($x =~ m|^arn:aws:(\S+):(\S+):(\d+):service/(\S+)$|) {
        $f = $1;#ecs, others???
        $z = $2;#zone
        $a = $3;#account
        $c = $4;
    };
    
    return ($f, $z, $a, $c);
};

#'clusterArn' => 'arn:aws:ecs:eu-west-3:123456789012:cluster/services'
sub parse_clusterArn($self, $x) {
    if ($x =~ m|^arn:aws:(\S+):(\S+):(\d+):cluster/(\S+)$|) {
        return ($1, $2, $3, $4);
    };
    
    return;
};

#'group' => 'service:this-that-whatever',
sub parse_group($self, $x) {
    if ($x =~ m|^service:(\S+)$|) {
        return $1
    };
    
    return undef;
};

1;
