package Camel::Util::Logger;
use strict;
use warnings;

use Data::Dumper;

use Moo;
use experimental qw/signatures/;

###############################################################################
sub _pre_format( $self, $x ) { $x =~ s/\n/ /g; $x =~ tr/  / /ds; return $x; }

sub _print_line( $self, $x ) {
    my $y = $self->_pre_format($x);
    print STDERR '[' . time . '] ' . "$y\n";
    return $y;
}

###############################################################################
sub generic_log ( $self, $x ) { return $self->_print_line($x); }
sub log_info    ( $self, $x ) { return $self->generic_log('_INFO: ' . $x); }
sub log_debug   ( $self, $x ) { return $self->generic_log('DEBUG: ' . $x); }
sub log_error   ( $self, $x ) { return $self->generic_log('ERROR: ' . $x); }
sub log_fatal   ( $self, $x ) {        $self->generic_log('FATAL: ' . $x); die "dying because $x"; };

###############################################################################
1;
