package Camel::Util::Jira;
use strict;
use warnings;

use Data::Dumper;
use Carp qw/croak/;
use MIME::Base64 qw/encode_base64/;
use JSON::MaybeXS qw/decode_json/;

use Moo;
use experimental 'signatures';

sub get_issue_details($self, $ctx, $issue, $api_endpoint, $api_username, $api_token) {

    croak "__PACKAGE__ issue is not defined" unless $issue;
    croak "__PACKAGE__ issue is not a scalar" if ref $issue;
    croak "__PACKAGE__ bad jira config" unless ($api_endpoint && $api_token && $api_username);

    my $url = $api_endpoint . '/issue/' . $issue;
    my $b64 = encode_base64($api_username . ':' . $api_token, '');

    my $h = { 'Authorization' => 'Basic ' . $b64 };
    my $r = $ctx->ua->request('GET', $url, { headers => $h } );
    
    my $details;
    if ($r->{success}) {
        my $c = $r->{content};
        $details = eval { return decode_json($c); };
    };

    $ctx->logger->log_error(__PACKAGE__ . ' nothing found in Jira for issue ' . $issue) unless $details->{fields};
    return $details;
};

1;
