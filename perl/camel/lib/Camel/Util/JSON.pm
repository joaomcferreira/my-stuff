package Camel::Util::JSON;
use strict;
use warnings;

use JSON::PP;
use Path::Tiny;

use Moo;
use experimental qw/signatures/;

sub enkode_json($self, $ctx, $d) {
    my $x = JSON::PP->new->ascii->pretty->allow_nonref;
    my $j = $x->encode($d);
    return $j;
};

sub dekode_json($self, $ctx, $j) {
    my $x = JSON::PP->new->ascii->pretty->allow_nonref;
    my $d = $x->decode($j);
    return $d;
};

sub save_json_file($self, $ctx, $d, $path) {
    my $j = $self->enkode_json($ctx, $d);
    my $r = path($path)->spew_raw($j);
    return $r;
};

sub load_json_file($self, $ctx, $path) {
    my $j = path($path)->slurp_utf8;
    my $d = $self->dekode_json($ctx, $j);
    return $d;
};

sub copy_ref_data($self, $ctx, $d) {
    my $c = $self->dekode_json($ctx, $self->enkode_json($ctx, $d));
    return $c;
};

1;
