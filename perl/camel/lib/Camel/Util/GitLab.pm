package Camel::Util::GitLab;
use strict;
use warnings;

use Data::Dumper;
use Carp qw/croak/;
use JSON::MaybeXS qw/decode_json/;

use Moo;
use experimental qw/signatures/;

###############################################################################
# jmf@debian10i:~$ echo $FOO
# PRIVATE-TOKEN: oy-zUZtLZ3BeTLkUec_r
#
# jmf@debian10i:~$ curl --header "$FOO" https://git.zzzzzz.net/api/v4/projects/14/repository/branches 2>/tmp/foo | jq .[0].name
# "XYZ-491-whatevere"
#
# jmf@debian10i:~$ curl --header "$FOO" https://git.zzzzzz.net/api/v4/projects/14/repository/branches 2>/tmp/foo | jq .[1].name
# "master"
#
###############################################################################

sub _internal_http_request($self, $ctx, $url_path, $gitlab_api, $gitlab_token_secret) {
    croak "__PACKAGE__ bad gitlab config" unless ($gitlab_api && $gitlab_token_secret);

    my $u = $gitlab_api . $url_path;
    my $h = { 'PRIVATE-TOKEN' => $gitlab_token_secret };
    
    $ctx->logger->log_info(__PACKAGE__ . ' GET ' . $u);
    my $r = $ctx->ua->request('GET', $u, { headers => $h } );
    $ctx->logger->log_error(__PACKAGE__ . ' GET error ' . Dumper($r)) unless $r->{success};
    return decode_json $r->{content};
}

sub _force_array_ref($self, $x) {
    return [] unless ($x && 'ARRAY' eq ref $x);
    return $x;
};

###############################################################################
sub repository_commits($self, $ctx, $p_id, $max, $branch, $gitlab_api, $gitlab_token_secret) {
    my $u = '/api/v4/projects/' . $p_id . '/repository/commits?ref_name=' . $branch;

    my $all = $self->_internal_http_request($ctx, $u, $gitlab_api, $gitlab_token_secret);
    $all = $self->_force_array_ref($all);

    my @some = splice @$all, 0, $max;
    return \@some;
};

sub repository_branches($self, $ctx, $p_id, $max, $gitlab_api, $gitlab_token_secret) {
    my $u = '/api/v4/projects/' . $p_id . '/repository/branches';

    my $all = $self->_internal_http_request($ctx, $u, $gitlab_api, $gitlab_token_secret);
    $all = $self->_force_array_ref($all);

    my @sorted = sort { $b->{commit}->{committed_date} cmp $a->{commit}->{committed_date} } @$all;
    my @some = splice @sorted, 0, $max;
    return \@some;
};

sub repository_compare($self, $ctx, $p_id, $f, $t, $gitlab_api, $gitlab_token_secret) {
    my $u = '/api/v4/projects/'.$p_id.'/repository/compare?from='.$f.'&to='.$t;
    my $r = $self->_internal_http_request($ctx, $u, $gitlab_api, $gitlab_token_secret);
    return $r;
};

###############################################################################
1;
