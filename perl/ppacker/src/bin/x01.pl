#!/usr/bin/env perl

use strict;
use warnings;

# to run: src/bin/x01.pl Shawn

use lib 'src/lib';
use SomePackage;

my $a = $ARGV[0] ? $ARGV[0] : 'Mr. ARGV_0';
print "Good day $a, how do you do ?\n";
SomePackage::SayHello( $a );
