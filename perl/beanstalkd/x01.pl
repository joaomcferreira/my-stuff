use strict;
use warnings;

use Beanstalk::Client;
use Data::Dumper;

#
#client
my $client = Beanstalk::Client->new( { server => 'localhost', default_tube => 'mine' });
print STDERR Dumper $client;

#
#send some messages
foreach my $d (5, 2, 1, 6) {
  my $data = 'msg_' . time . "_$d";
  $data = '{ "sheep": "Schawn" }' if $d == 6;
  print STDERR "put $data\n";
  $client->put( { data => $data, priority => 100, ttr => 120, delay => $d } );
  sleep 1;
}

#
#receive messages while they exist
while (1) {
  my $work = $client->reserve(9);
  print STDERR "no more work\n" unless $work;
  last unless $work;

  sleep 1;

  my $job_id = $work->{id};

  my $delete_ok;
  $delete_ok = $client->delete($job_id) if $job_id;

  print STDERR "|job_id: $job_id|job_data: $work->{data}|delete: $delete_ok|t = " . time . "|\n";
}

print STDERR "bye, bye (at t = " . time . ")\n";
