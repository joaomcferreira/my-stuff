https://metacpan.org/pod/Beanstalk::Client
https://peterstuifzand.nl/2009/03/29/simple-beanstalkd-example.html

docker build -t btk .
docker run -p 11300:11300 btk

deb: libyaml-syck-perl, libsocket-perl
cpanm -v --installdeps .
perl x.pl

TODO:
- this looks cool: https://metacpan.org/pod/AnyEvent::Beanstalk
