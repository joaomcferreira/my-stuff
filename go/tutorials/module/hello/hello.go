package main

import (
    "fmt"
    "log"
    "example.com/greetings"
    "time"
)

func main() {
    tnow := time.Now()
    fmt.Println(tnow)
    nano := tnow.UnixNano()
    fmt.Println(nano)

    log.SetPrefix("greetings: ")
    log.SetFlags(0)

    message1, err1 := greetings.Hello("Gladyx")
    if err1 != nil {
        log.Fatal(err1) //exits
    }
    fmt.Println(message1)

    message2, err2 := greetings.Hello("")
    if err2 != nil {
        //log.Fatal(err2) //exits
        fmt.Println("smells fishy !!!")

    }
    fmt.Println(message2)

    names := []string{"Filipe", "Alex", "Xone"}

    // Request greeting messages for the names.
    messages, err3 := greetings.Hellos(names)
    if err3 != nil {
        log.Fatal(err3) //exits
    }
    fmt.Println(messages)
}
