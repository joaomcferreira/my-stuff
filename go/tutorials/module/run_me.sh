#!/bin/bash
echo '========================='
echo '======== testing ========'
echo '========================='
cd greetings && go test
echo '========================='
echo '======== running ========'
echo '========================='
cd ../hello && go run .
echo '========================='
echo '======= compiling ======='
echo '========================='
cd ../hello && rm -rf hello && go build && ./hello
echo '========================='
