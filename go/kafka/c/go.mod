module joaomcferreira/learning/golang-a03-ifc-n01

go 1.15

require (
	github.com/confluentinc/confluent-kafka-go v1.9.2
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/uuid v1.3.0
	github.com/klauspost/compress v1.12.2 // indirect
	github.com/segmentio/kafka-go v0.4.28
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.8.2 // indirect
)
