package main

import (
    "fmt"
    "time"
    "encoding/json"
)

func main() {
    logger  := &basic_logger{}
    hwork   := &basic_worker{}
    kafka_a := &mqueue_kafka_a{}
    kafka_b := &mqueue_kafka_b{}
    mq_cnfg := &mqueue_cfg_x{}

    dc_A := ctx{ "Kafka driver A", logger, kafka_a, mq_cnfg, hwork }
    dc_B := ctx{ "Kafka driver B", logger, kafka_b, mq_cnfg, hwork }

    topic := "low_pri_tasks"

    tx_n_messages (dc_B, topic, 4)// send 4 messages with driver B ...
    rx_n_messages (dc_A, topic, 3)// and receive 3 of them with driver A.
    tx_n_messages (dc_A, topic, 4)// send 4 more messages with driver A...
    rx_n_messages (dc_B, topic, 3)// and receive 3 more with driver B.
}

func tx_n_messages (dc ctx, topic string, total int) {
    dc.logger.log_warn(fmt.Sprintf("Sending Messages with %v", dc.name))
    for i := 1; i <= total; i++ {
        key      := fmt.Sprintf("Key_%v", i)
        txmsg    := FarmTaskMessage{ 1000 + i, "Joe", fmt.Sprintf("feed the pigs [%v]", i) }
        bytes, _ := json.Marshal(txmsg)
        dc.mqueue.tx_one_msg(dc, topic, key, bytes)
        time.Sleep(1 * time.Second)
    }
}

func rx_n_messages (dc ctx, topic string, max_rx int) {
    dc.logger.log_warn(fmt.Sprintf("Receiving Messages with %v", dc.name))
    dc.mqueue.rx_msgs(dc, topic, max_rx)
}
