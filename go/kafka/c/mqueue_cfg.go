package main

///////////////////////////////////////////////////////////////////////////////
// public (imqcnfg interface)
///////////////////////////////////////////////////////////////////////////////
type mqueue_cfg_x struct {}

func (q *mqueue_cfg_x) get_host (dc ctx) string {
    return "localhost:9092" // in some scenarios this could be reading some env variable
                            // established on program startup or on docker run, usual approach
}

func (q *mqueue_cfg_x) get_topic (dc ctx) string {
    return "farm_tasks_not_urgent"
}
