package main

import (
    "fmt"
    "os"
)

type ilogger interface {
    log_info(string) string
    log_warn(string) string
    log_error(string) string
    log_FATAL(string)
}

type basic_logger struct {}

func (l *basic_logger) log_info(text string) string {
    fmt.Println("_INFO: " + text)
    return "i"
}

func (l *basic_logger) log_warn(text string) string {
    fmt.Println("_WARN: " + text)
    return "i"
}

func (l *basic_logger) log_error(text string) string {
    fmt.Println("ERROR: " + text)
    return "e"
}

func (l *basic_logger) log_FATAL(text string) {
    fmt.Println("FATAL: " + text + ". Dying.")
    os.Exit(1)
}
