package main

type ctx struct {
    name string
    logger ilogger
    mqueue imqueue
    mq_cfg imqcnfg
    worker iworker
}
