package main

import (
    "fmt"
    "os/exec"
)

type basic_worker struct {}

func (l *basic_worker) handle(dc ctx, message string) string {
    dc.logger.log_info("WORKER handling message: " + message)

    echo := fmt.Sprintf("echo '%v' >> /tmp/foo.txt", message)

    cmd := exec.Command("bash", "-c", echo)
    _, e := cmd.Output()

    if e != nil {
        fmt.Println(e.Error())
    }

    return "whatever"
}
