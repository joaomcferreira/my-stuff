package main

type imqueue interface {
    tx_one_msg(ctx, string, string, []byte) string
    rx_msgs(ctx, string, int) string
}

type imqcnfg interface {
    get_host (ctx) string
    get_topic(ctx) string
}

type FarmTaskMessage struct {
    Id   int
    Who  string
    Task string
}

// Message to carry any desired text.
// Could be JSON, Base64, XML, YAML or human text.
// The Data field contains a string of readable
// characters. The MIME and Type fields can be used
// to specify what the Data content or context is.
type GenericTextMessage struct {
    MIME string
    Type string
    Data string
}
