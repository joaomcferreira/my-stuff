package main

import (
    "fmt"
    "time"
    "strings"
    "context"
    kfk "github.com/segmentio/kafka-go"
)

///////////////////////////////////////////////////////////////////////////////
// public (imqueue interface)
///////////////////////////////////////////////////////////////////////////////
type mqueue_kafka_b struct {}

func (q *mqueue_kafka_b) tx_one_msg(dc ctx, topic string, key string, msg []byte) string {
    dc.logger.log_warn("KAFKA Tx; topic: " + topic + "; msg: |" + string(msg) + "|")
    _send_message_to_kafka_b(dc, topic, key, msg)
    return "TODO_jmf_01"
}

func (q *mqueue_kafka_b) rx_msgs(dc ctx, topic string, max int) string {
    dc.logger.log_warn("KAFKA rx_msgs; topic: " + topic)
    _receive_messages_from_kafka_b(dc, topic, max)
    return "TODO_jmf_02"
}

///////////////////////////////////////////////////////////////////////////////
// private (implementation functions)
///////////////////////////////////////////////////////////////////////////////
func _send_message_to_kafka_b(dc ctx, topic string, key string, msg []byte) int {
    dc.logger.log_info("producer create (" + dc.name + ")")

    mq_host_fqdn := dc.mq_cfg.get_host(dc)

    writer := _newKafkaWriter(mq_host_fqdn, topic)
    defer writer.Close()

    kafka_msg := kfk.Message{ Key: []byte(key), Value: msg }

    err := writer.WriteMessages(context.Background(), kafka_msg)
    if err == nil {
        dc.logger.log_info(fmt.Sprintf("write message ok (%v)", key))
    } else {
        dc.logger.log_FATAL(fmt.Sprintf("write message ERROR (%v) (Error: %v)", key, err))
        return -1
    }

    time.Sleep(1 * time.Second)
    return 1
}

func _receive_messages_from_kafka_b(dc ctx, topic string, max_msgs int) int {
    dc.logger.log_info("consumer create (" + dc.name + ")")

    mq_host_fqdn := dc.mq_cfg.get_host(dc)
    reader := _getKafkaReader(mq_host_fqdn, topic, "group1")
    defer reader.Close()

    dc.logger.log_warn(fmt.Sprintf("consumer create ok (%v)", topic))

    for i := 1; i <= max_msgs; i++ {
        m, err := reader.ReadMessage(context.Background())
        if err == nil {
            message_received := string(m.Value)
            dc.logger.log_info(
                fmt.Sprintf(
                    "ReadMessage ok at topic:%v[%v]@%v, %s (%s)",
                            m.Topic, m.Partition, m.Offset, string(m.Key), message_received))
            dc.worker.handle(dc, message_received)
        } else {
            dc.logger.log_error(fmt.Sprintf("ReadMessage ERROR (%v)", err))
        }
    }

    return 1
}

/////////////////////////////////////////////////////////////////////////////
func _newKafkaWriter(url string, topic string) *kfk.Writer {
    return &kfk.Writer{
        Addr:     kfk.TCP(url),
        Topic:    topic,
        Balancer: &kfk.LeastBytes{},
        RequiredAcks: kfk.RequireAll,
    }
}

func _getKafkaReader(hosts, topic, group string) *kfk.Reader {
    return kfk.NewReader(
        kfk.ReaderConfig{
            Brokers:  strings.Split(hosts, ","),
            GroupID:  group,
            Topic:    topic,
            MinBytes: 10e1,
            MaxBytes: 10e6,
        })
}
