wip...
===

* https://www.tutorialsbuddy.com/write-data-to-a-kafka-topic-in-go-example

* https://developer.confluent.io/quickstart/kafka-docker/

* https://github.com/confluentinc/confluent-kafka-go/tree/master/examples/consumer_example

* https://gist.github.com/edenhill/9dfc019b980a5eb3365c84524a1f12b0
___

* go get -u gopkg.in/confluentinc/confluent-kafka-go.v1/kafka

go: downloading gopkg.in/confluentinc/confluent-kafka-go.v1 v1.8.2

go: downloading github.com/confluentinc/confluent-kafka-go v1.9.2

go: added github.com/confluentinc/confluent-kafka-go v1.9.2

go: added gopkg.in/confluentinc/confluent-kafka-go.v1 v1.8.2
___

* To run: 'go run .'
___

Libs
====

* https://pkg.go.dev/github.com/segmentio/kafka-go

* https://github.com/segmentio/kafka-go/blob/main/examples/producer-random

GUIs
====

* https://github.com/Consdata/kouncil/

* https://github.com/provectus/kafka-ui
