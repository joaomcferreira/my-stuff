example a
=====

The same golang program sends and receives some Kafka messages.

- console 1: Start Kafka
  * `docker-compose up`

- console 2: Send/Receive Kafka messages
  * `cd a && go run .`
  * will first send two messages to Kafka
  * and then receive one message from Kafka

Also does some playing arount with a 'bunny' message queue which is actually just a mock doing nothing in order to clarify the principles of dependency injection and interfaces.

example b
=====

One golang program sends messages to Kafka. Another golang program receives messages from Kafka.

- console 1: Start Kafka
  * `docker-compose up`

- console 2: Send messages
  * `cd b && go run . tx`
  * sends 7 messages to Kafka

- console 3: Receive messages
  * `cd b && go run . rx`
  * receives up to 5 messages from Kafka

example c
=====

The same golang program sends and receives Kafka messages using 2 distinct drivers:

- driver A: "github.com/confluentinc/confluent-kafka-go/kafka"

- driver B: "github.com/segmentio/kafka-go"

Upon reception, messages are passed to a handler to simulate the 'worker' concept: a program that is supposed to execute some task or some calculation, upon reception of a message from a queue.

The worker task is simply, for ilustration purposes, to append the received message to a text file, by invoking a shell command.

- console 1: Start Kafka
  * `docker-compose up`

- console 2: Send/Receive Kafka messages
  * `cd c && go run .`
  * a) sends 4 messages using driver A
  * b) receives 3 messages with driver B
  * c) sends 4 more messages with driver B
  * d) receives 3 messages with driver A
