package main

import (
    "fmt"
    "github.com/confluentinc/confluent-kafka-go/kafka"
)

///////////////////////////////////////////////////////////////////////////////
// public (imqueue interface)
///////////////////////////////////////////////////////////////////////////////
type kafka_mqueue struct {}

func (q *kafka_mqueue) tx_one_msg(dc ctx, topic string, msg []byte) {
    dc.logger.log_warn("KAFKA Tx; topic: " + topic + "; msg: |" + string(msg) + "|")
    _send_message_to_kafka(dc, topic, msg)
}

func (q *kafka_mqueue) rx_one_msg(dc ctx, topic string) string {
    dc.logger.log_warn("KAFKA rx_one_msg; topic: " + topic)
    return _receive_messages_from_kafka(dc, topic, 1)
}

func (q *kafka_mqueue) rx_msgs(dc ctx, topic string, max int) string {
    dc.logger.log_warn("KAFKA rx_msgs; topic: " + topic)
    return _receive_messages_from_kafka(dc, topic, max)
}

///////////////////////////////////////////////////////////////////////////////
// private (implementation functions)
///////////////////////////////////////////////////////////////////////////////
func _send_message_to_kafka(dc ctx, topic string, msg []byte) {

    dc.logger.log_warn("Starting producer...\n")
    p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": "localhost:9092"})
    if err != nil {
        panic(err)
    }

    defer p.Close()

    // Delivery report handler for produced messages
    go func() {
        for e := range p.Events() {
            switch ev := e.(type) {
            case *kafka.Message:
                if ev.TopicPartition.Error != nil {
                    fmt.Printf("--> Message Send Failed (TopicPartition: %v)\n", ev.TopicPartition)
                } else {
                    fmt.Printf("--> Message Send Ok (TopicPartition: %v)\n", ev.TopicPartition)
                }
            }
        }
    }()

    p.Produce(
        &kafka.Message{
            TopicPartition: kafka.TopicPartition{
                Topic: &topic,
                Partition: kafka.PartitionAny},
            Value: msg,
        },
        nil)

    // Wait for message deliveries before shutting down
    p.Flush(15 * 1000)
}

func _receive_messages_from_kafka(dc ctx, topic string, max_msgs int) string {

    dc.logger.log_warn("Consumer create...")

    c, err := kafka.NewConsumer(&kafka.ConfigMap{
        "bootstrap.servers": "localhost:9092",
        "broker.address.family":    "v4",
        "group.id":                 "group1",
        "session.timeout.ms":       6000,
        "auto.offset.reset":        "earliest",
        "enable.auto.offset.store": false,
    })

    if err != nil {
        fmt.Printf("Failed to create consumer: %s\n", err)
        return "Failed to create consumer.";
    }

    dc.logger.log_warn(fmt.Sprintf("Consumer Create Ok (%v)", c))

    topics := []string{ topic }
    err = c.SubscribeTopics(topics, nil)
    dc.logger.log_warn(fmt.Sprintf("Consumer Subscribed Topic (%v)", topic))

    poll_time := 500
    max_loops := 20
    cnt_loops := 0
    cnt_msgs  := 0

    for cnt_loops < max_loops {
        cnt_loops += 1
        select {
            default:

            dc.logger.log_warn(fmt.Sprintf("Poll() %v/%v", cnt_loops, max_loops))
            ev := c.Poll(poll_time)
            if ev == nil {
                dc.logger.log_warn("Poll returns nil; nothing to do.")
                continue
            }

            n_received := handle_kafka_consumer_event(dc, c, ev)

            if n_received < 0 {
                cnt_loops = max_loops
            } else {
                cnt_msgs += n_received
            }

            if cnt_msgs >= max_msgs {
                cnt_loops = max_loops
            }

        }
    }

    dc.logger.log_warn("Consumer Close")
    c.Close()
    return fmt.Sprintf("received %v messages", cnt_msgs)
}

func handle_kafka_consumer_event(dc ctx, c *kafka.Consumer, ev kafka.Event) int {

    ret_val := 0 // means: 0 messages received and no errors found

    switch e := ev.(type) {
        case *kafka.Message:
            message_received := string(e.Value)
            fmt.Printf("--> Got a new message (TopicPartition: %s)\n", e.TopicPartition)
            fmt.Printf("----> Message: %s\n", message_received)
            _, err := c.StoreMessage(e)
            if err == nil {
                ret_val = 1 // means: 1 message received and no errors found
                dc.worker.handle(dc, message_received)
            } else {
                ret_val = -1 // means error
                dc.logger.log_error(fmt.Sprintf("Error storing offset (TopicPartition: %s)", e.TopicPartition))
            }
        case kafka.Error:
            ret_val = -2 // also means error
            fmt.Printf("--> ERROR [%v] [%v]\n", e.Code(), e)
        default:
            ret_val = -9 // means weird...
            fmt.Printf("--> Weird things happen !! ... maybe no more messages in queue ?! ... [%v]\n", e)
    }

    return ret_val
}
