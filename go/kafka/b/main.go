package main

import (
    "os"
    "fmt"
    "time"
    "encoding/json"
)

func main() {

    arg := valid_arg_or_exit_1 ()

    topic  := "high_pri_tasks"
    logger := &basic_logger{}
    kafka  := &kafka_mqueue{}
    hwork  := &basic_worker{}
    dc     := ctx{ "Testing Kafka", logger, kafka, hwork }

    if (arg == "tx") {
        tx_n_messages (dc, topic, 7)
    } else {
        rx_n_messages (dc, topic, 2)
    }

}

func valid_arg_or_exit_1 () string {
    if (len(os.Args) < 2) {
        fmt.Println("ERROR. Need cmd line argument ('tx' for producer, 'rx' for consumer)")
        os.Exit(1)
    }

    arg := os.Args[1]

    if arg != "tx" && arg != "rx" {
        fmt.Println("ERROR. Bad cmd line argument ('tx' for producer, 'rx' for consumer)")
        os.Exit(1)
    }

    return arg
}

func tx_n_messages (dc ctx, topic string, total int) {
    dc.logger.log_warn ("========== Sending Messages ========================")
    for i := 1; i <= total; i++ {
        txmsg := FarmTaskMessage{ 1000 + i, "Joe", fmt.Sprintf("feed the pig [%v]", i) }
        bytes, _ := json.Marshal(txmsg)
        dc.mqueue.tx_one_msg(dc, topic, bytes)
        time.Sleep(1 * time.Second)
    }
}

func rx_n_messages (dc ctx, topic string, max_rx int) {
    dc.logger.log_warn ("========== Receiving Messages ======================")

    //receive up to 1 message
    rx_log := dc.mqueue.rx_one_msg(dc, topic)
    dc.logger.log_info(rx_log)
        time.Sleep(time.Second * 1)

    // receive up to 2 messages on each max_rx loops
    for i := 1; i <= max_rx; i++ {
        rx_log := dc.mqueue.rx_msgs(dc, topic, 2)
        dc.logger.log_info(rx_log)
        time.Sleep(time.Second * 1)
    }
}
