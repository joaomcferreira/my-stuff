package main

type basic_worker struct {}

func (l *basic_worker) handle(dc ctx, message string) string {
    dc.logger.log_info("WORKER handling message: " + message)
    return "whatever"
}
