package main

import (
    "fmt"
    "github.com/confluentinc/confluent-kafka-go/kafka"
)

///////////////////////////////////////////////////////////////////////////////
// public (imqueue interface)
///////////////////////////////////////////////////////////////////////////////
type kafka_mqueue struct {}

func (q *kafka_mqueue) tx_one_msg(topic string, msg []byte) {
    fmt.Println("KAFKA Tx; topic: " + topic + "; msg: |" + string(msg) + "|")
    _send_message_to_kafka(topic, msg)
}

func (q *kafka_mqueue) rx_one_msg(topic string) string {
    fmt.Println("KAFKA Rx; topic: " + topic)
    return _receive_message_from_kafka(topic)
}

///////////////////////////////////////////////////////////////////////////////
// private (implementation functions)
///////////////////////////////////////////////////////////////////////////////
func _send_message_to_kafka(topic string, msg []byte) {

    fmt.Printf("Starting producer...\n")
    p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": "localhost:9092"})
    if err != nil {
        panic(err)
    }

    defer p.Close()

    // Delivery report handler for produced messages
    go func() {
        for e := range p.Events() {
            switch ev := e.(type) {
            case *kafka.Message:
                if ev.TopicPartition.Error != nil {
                    fmt.Printf("--> Message Send failed: %v\n", ev.TopicPartition)
                } else {
                    fmt.Printf("--> Message Send Ok (TxTopic: %v)\n", ev.TopicPartition)
                }
            }
        }
    }()

    p.Produce(
        &kafka.Message{
            TopicPartition: kafka.TopicPartition{
                Topic: &topic,
                Partition: kafka.PartitionAny},
            Value: msg,
        },
        nil)

    // Wait for message deliveries before shutting down
    p.Flush(15 * 1000)
}

func _receive_message_from_kafka(topic string) string {

    topics := []string{ topic }

    c, err := kafka.NewConsumer(&kafka.ConfigMap{
        "bootstrap.servers": "localhost:9092",
        "broker.address.family":    "v4",
        "group.id":                 "group1",
        "session.timeout.ms":       6000,
        "auto.offset.reset":        "earliest",
        "enable.auto.offset.store": false,
    })

    if err != nil {
        fmt.Printf("Failed to create consumer: %s\n", err)
        return "Failed to create consumer.";
    }

    fmt.Printf("Created Consumer %v\n", c)

    err = c.SubscribeTopics(topics, nil)

    message_received := "NOTHING"

    run := true

    for run {
        select {
        default:
            ev := c.Poll(100)
            if ev == nil {
                continue
            }

            switch e := ev.(type) {
            case *kafka.Message:
                message_received = string(e.Value)
                fmt.Printf("--> Got a new message (RxTopic: %s)\n", e.TopicPartition)
                fmt.Printf("----> Message: %s\n", message_received)
                if e.Headers != nil {
                    fmt.Printf("%% Headers: %v\n", e.Headers)
                }
                _, err := c.StoreMessage(e)
                if err != nil {
                    fmt.Printf("%% Error storing offset after message %s:\n",
                        e.TopicPartition)
                }
            case kafka.Error:
                fmt.Printf("--> ERROR [%v] [%v]\n", e.Code(), e)
                if e.Code() == kafka.ErrAllBrokersDown {
                    run = false
                }
                run = false
            default:
                run = false
                fmt.Printf("--> ...weird things happen... (%v)\n", e)
            }
            run = false
        }
        run = false
    }

    fmt.Printf("Closing consumer\n")
    c.Close()
    return message_received
}
