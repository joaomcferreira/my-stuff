module joaomcferreira/learning/golang-a03-ifc-n01

go 1.15

require (
	github.com/confluentinc/confluent-kafka-go v1.9.2
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.8.2 // indirect
)
