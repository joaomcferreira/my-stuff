package main

import (
  "fmt"
  "time"
  "encoding/json"
)

func main() {
    lg := &basic_logger{}

    bunny := &bunny_mqueue{}
    ctx_a := ctx{"Testing Bunny", lg, bunny}
    do_some_messaging(ctx_a, "test sending messages to Bunny")

    kafka := &kafka_mqueue{}
    ctx_b := ctx{"Testing Kafka", lg, kafka}
    do_some_messaging(ctx_b, "test sending messages to Kafka")
}

func do_some_messaging (dc ctx, what string) {
    topic := "not_urgent_stuff"
    xtime := fmt.Sprintf("%10d", time.Now().Unix())

    dc.logger.log_info ("====================================================")
    dc.logger.log_info ("I will " + what + " (" + dc.name + ")")


    dc.logger.log_info ("===== send one message =============================")
    tx_msg := FarmTaskMessage{567, "Feed the Horse", xtime, "Shawn","Hay+Corn"}
    tx_bytes, _ := json.Marshal(tx_msg)
    dc.mqueue.tx_one_msg(topic, tx_bytes)

    dc.logger.log_info ("===== send another message =========================")
    tx_msg = FarmTaskMessage{568, "Feed the Pigs", xtime, "Joe", "Vegetables" }
    tx_bytes, _ = json.Marshal(tx_msg)
    dc.mqueue.tx_one_msg(topic, tx_bytes)

    dc.logger.log_info ("===== receive one message ==========================")
    rx_msg := dc.mqueue.rx_one_msg(topic)
    dc.logger.log_info("received message: " + rx_msg)

    dc.logger.log_info ("====================================================")
    dc.logger.log_info ("")
}
