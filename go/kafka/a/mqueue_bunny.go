package main

import (
    "fmt"
)

type bunny_mqueue struct {}

func (q *bunny_mqueue) tx_one_msg(topic string, msg []byte) {
    fmt.Println("bunny Tx; topic: " + topic)
}

func (q *bunny_mqueue) rx_one_msg(topic string) string {
    response := "bunny Rx; topic: " + topic + ";"
    fmt.Println(response)
    return response
}
