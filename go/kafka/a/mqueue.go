package main

//TODO_jmf: add the ctx as argument on all function
//   signatures in order to, for example, all functions
//   have access to the same logger... nothing new...

type imqueue interface {
    tx_one_msg(string, []byte)
    rx_one_msg(string) string
}

type FarmTaskMessage struct {
    Id       int
    Task     string
    When     string
    Assignee string
    Notes    string
}

// Message to carry any desired text.
// Could be JSON, Base64, XML, YAML or human text.
// The Data field contains a string of readable
// characters. The MIME and Type fields can be used
// to specify what the Data content or context is.
type GenericTextMessage struct {
    MIME string
    Type string
    Data string
}
