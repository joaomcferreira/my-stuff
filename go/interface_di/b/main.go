package main

import "fmt"

/////////////// interface ictx ////////////////////////////////////////////////
type ictx interface {
    name() string
}

/////////////// ctx_prod //////////////////////////////////////////////////////
type ctx_prod struct {
    nm string
}

func (c *ctx_prod) name() string {
    return c.nm
}

/////////////// ctx_test //////////////////////////////////////////////////////
type ctx_test struct {
    my_name string
}

func (c *ctx_test) name() string {
    return c.my_name
}

/////////////// work //////////////////////////////////////////////////////////
func do_things (c ictx, what string) {
    fmt.Println("do_things: " + c.name())
}

/////////////// main //////////////////////////////////////////////////////////
func main() {
    fmt.Println("Hello, World!")

    my_ctx_prod := &ctx_prod { "MyContext_A" }
    do_things(my_ctx_prod, "save the world and go to heaven")

    my_ctx_test := &ctx_test { "MyContext_B" }
    do_things(my_ctx_test, "save the world and go to heaven")
}

///////////////////////////////////////////////////////////////////////////////
