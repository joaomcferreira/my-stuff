package main

func do_things (c ctx, what string) {
    c.logger().log_info("Context Based Dependency Injection with Golang")
    c.logger().log_info(c.name())
    c.logger().log_error(what)
    c.logger().log_info("not really an error (we're just testing)")
    c.mqueue().tx_one_msg("not_urgent_stuff", "just saying hello")
    message := c.mqueue().rx_one_msg("urgency_list")
    c.logger().log_info("received from mqueue: " + message)
}
