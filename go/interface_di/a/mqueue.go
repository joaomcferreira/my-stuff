package main

import "fmt"

type simple_mqueue struct {}

func (q *simple_mqueue) tx_one_msg(channel string, msg string) {
    fmt.Println("simple_mqueue tx; channel: " + channel + "; msg: " + msg + ";")
}

func (q *simple_mqueue) rx_one_msg(channel string) string {
    blah := "simple_mqueue rx; channel: " + channel + ";"
    fmt.Println(blah)
    return blah
}
