package main

import "fmt"
import "time"

func main() {
    banner := "======================="

    fmt.Println("Hello, World!")

    time_now_unix := time.Now().Unix()
    fmt.Printf("epoch is %d\n", uint64(time_now_unix))

    // logger object
    my_logger := &basic_logger{}
    my_logger.log_info(banner)

    // messaging object
    my_mqueue := &simple_mqueue{}

    // context object (aka dependency injection container)
    my_ctx := ctx { "MyContext", my_logger, my_mqueue }

    // injecting the dependency(ies) needed to do_things
    do_things(my_ctx, "save the world and go to heaven")

    my_ctx.logger().log_info(banner)
}
