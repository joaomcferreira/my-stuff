package main

type ctx struct {
    nm string
    lg *basic_logger
    mq *simple_mqueue
}

func (c *ctx) name() string {
    return c.nm
}

func (c *ctx) logger() *basic_logger {
    return c.lg
}

func (c *ctx) mqueue() *simple_mqueue {
    return c.mq
}
