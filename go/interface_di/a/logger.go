package main

import "fmt"

type basic_logger struct {}

func (l *basic_logger) log_error(text string) {
    fmt.Println("ERROR: " + text)
}

func (l *basic_logger) log_info(text string) {
    fmt.Println("_INFO: " + text)
}
