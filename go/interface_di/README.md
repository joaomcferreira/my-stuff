Dependency Injection and Interfaces
===

These are some simple illustrations of the usage of a Dependency Injection Container (DiC, ctx) on top of Golang Interfaces.

* example a - ctx is a struct composed of structs, each pointing to a software module required by the application;
* example b - ctx is an interface; distinct ctx implementations are used in the distinct scenarios, required by the application;
* example c - ctx is a struct composed of interfaces; one interface for each software module required by the application; in disctinct scenarios the actual software modules will differ, but they share the same interface; the adherence to the interface definition is ensured by golang.

I usually find example c the most powerful and subject to less limitations.

To run: 'go run .'

Enjoy

João, 20221231, Happy New Year !!!
