package main

func main() {
    prd_logger := &basic_logger{}
    prd_mqueue := &fakfa_mqueue{}
    tst_mqueue := &bunny_mqueue{}

    prod_ctx := ctx{ "Production Ctx"  , prd_logger, prd_mqueue }
    test_ctx := ctx{ "Unit Testing Ctx", prd_logger, tst_mqueue }

    do_your_work(prod_ctx, "save the world and go to heaven")
    do_your_work(test_ctx, "test some functions and go home")
}
