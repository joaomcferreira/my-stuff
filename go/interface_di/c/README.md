Dependency Injection and Interfaces
===

These are some illustrations of the usage of a Dependency Injection Container (DiC, ctx) on top of Golang Interfaces.

In example "c", the DiC is a struct composed of interfaces: each of them provides access to a software module required by the application.

In disctinct scenarions the actual software modules will differ, but they share the same interface. The adherence to the interface definition is ensured by golang.

To run: 'go run .'
