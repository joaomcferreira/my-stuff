package main

import "fmt"

///////////////////////////// imqueue /////////////////////////////////////////
type imqueue interface {
    tx_one_msg(string, string)
    rx_one_msg(string) string
}

///////////////////////////// fakfa mqueue ///////////////////////////////////////////
type fakfa_mqueue struct {}

func (q *fakfa_mqueue) tx_one_msg(channel string, msg string) {
    fmt.Println("fakfa tx; channel: " + channel + "; msg: " + msg + ";")
}

func (q *fakfa_mqueue) rx_one_msg(channel string) string {
    blah := "fakfa rx; channel: " + channel + ";"
    fmt.Println(blah)
    return blah
}

///////////////////////////// bunny mqueue ////////////////////////////////////
type bunny_mqueue struct {}

func (q *bunny_mqueue) tx_one_msg(channel string, msg string) {
    fmt.Println("bunny Tx; channel: " + channel + "; msg: " + msg + ";")
}

func (q *bunny_mqueue) rx_one_msg(channel string) string {
    blah := "bunny Rx; channel: " + channel + ";"
    fmt.Println(blah)
    return blah
}

///////////////////////////////////////////////////////////////////////////////
