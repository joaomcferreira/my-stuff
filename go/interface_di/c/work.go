package main

func do_your_work (dc ctx, what string) {
    dc.logger.log_info ("=======================================================")
    do_some_logging(dc, what)
    dc.logger.log_info ("=======================================================")
    do_some_messaging(dc)
    dc.logger.log_info ("=======================================================")
    dc.logger.log_info ("")
}

func do_some_logging (dc ctx, what string) {
    dc.logger.log_info ("I will " + what)
    dc.logger.log_info ("with a dependency container named '" + dc.name + "'")
    dc.logger.log_error("not really...")
    dc.logger.log_info ("... all is fine !!!")
}

func do_some_messaging (dc ctx) {
    dc.logger.log_info ("sending and receiving messages")
    dc.mqueue.tx_one_msg("not_urgent_stuff", "just saying hello")
    message := dc.mqueue.rx_one_msg("urgency_list")
    dc.logger.log_info("received message " + message)
}
