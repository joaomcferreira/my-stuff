package main

import (
  "fmt"
  "log"
  "time"
  amqp "github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
  if err != nil {
    log.Panicf("%s: %s", msg, err)
  }
}

func main() {

  now := time.Now()
  eph := fmt.Sprintf("%10d", now.Unix())

  conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
  failOnError(err, "Failed to connect to RabbitMQ")
  defer conn.Close()

  ch, err := conn.Channel()
  failOnError(err, "Failed to open a channel")
  defer ch.Close()

  q, err := ch.QueueDeclare("hello", false, false, false, false, nil)
  failOnError(err, "Failed to declare a queue")

  body := "Hello World! (" + eph + ")"
  err = ch.Publish("",  q.Name, false, false,
    amqp.Publishing{
      ContentType: "text/plain",
      Body:        []byte(body),
    })
  failOnError(err, "Failed to publish a message")

  log.Printf(" [x] Sent %s\n", body)
}
