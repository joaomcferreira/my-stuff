A simple ilustrative setup of an HTTP enabled backend accessing an ArangoDB database, using node and express.

The deployment is docker based. The proposed docker image builds upon a base image with all node and npm stuff pre-installed and simply runs the tests.

Cheers

jmf, updated on 20220408
