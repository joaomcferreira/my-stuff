function is_even (num) {
  return num % 2 === 0;
}

function say_if_number_is_even (num) {
    var it_is_even = is_even(num);

    if (it_is_even) {
        console.log(num + ' is an even value');
    } else {
        console.log(num + ' is an odd value');
    };

    return it_is_even;
}

//exports.is_even = is_even;
exports.say_if_number_is_even = say_if_number_is_even;
