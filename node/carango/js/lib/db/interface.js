const { Database, aql } = require('arangojs');

function is_even (num) {
  return num % 2 === 0;
}

function say_if_number_is_even (num) {
    var it_is_even = is_even(num);

    if (it_is_even) {
        console.log(num + ' is an even value');
    } else {
        console.log(num + ' is an odd value');
    };

    return it_is_even;
}

async function connect (url, name, user, pass) {
    console.log("connecting...");
    const db = new Database({
        url: url,//"http://localhost:8529",
        databaseName: name,
        auth: { username: user, password: pass },
    });
    return db;
}

async function create_collection (db, coll_name) {
    const coll = db.collection(coll_name);
    const create_return = await coll.create();
    console.log("connected.");
    console.log("collection created: " + create_return.name);
    return coll;
}

exports.connect = connect;
exports.create_collection = create_collection;
//exports.is_even = is_even;
//exports.say_if_number_is_even = say_if_number_is_even;
