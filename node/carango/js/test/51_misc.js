var chai   = require('chai');
var expect = chai.expect;
var misc_ifc = require('../lib/misc/interface');

describe('misc stuff, wip, experimentations and such', function() {
  describe('basic integer operations and logic', function() {
    it('should be able to distinguish even integers', function(done) {
        const eight_is_even = misc_ifc.say_if_number_is_even(8);
        expect(eight_is_even).to.equal(true);
        done();
    });
    it('should be able to distinguish odd integers', function(done) {
        const seven_is_even = misc_ifc.say_if_number_is_even(7);
        expect(seven_is_even).to.equal(false);
        done();
    });
  });
});
