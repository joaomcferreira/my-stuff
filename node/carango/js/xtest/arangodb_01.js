var chai = require('chai');
var expect = chai.expect;
const { Database, aql } = require("arangojs");

describe('Database read/write access', function() {

    it('creates a collection, saves a document and reads it back', function(done) {
        async function main() {
            try {
                //connect to DB
                console.log("connecting...");
                const db = new Database({
                    url: "http://192.168.1.65:8529",
                    databaseName: "_system",
                    auth: { username: "root", password: "root" },
                });

                const time_now = Date.now();//UNIX time miliseconds
                const coll_name = "my_test_collection_" + time_now;

                //create a new collection
                const coll = db.collection(coll_name);
                const create_return = await coll.create();
                console.log("connected.");
                //
                console.log("collection created: " + create_return.name);
                expect(create_return.name ).to.equal(coll_name);
                expect(create_return.code ).to.equal(      200);
                expect(create_return.error).to.equal(    false);

                //save a new document
                const result = await coll.save( { a: "a", n: 123456, t: time_now }, { returnNew: true } );
                console.log("document saved: " + result._key);
                expect(result._key).to.be.a("string");//looks like an integer...
                expect(result.zzzz).to.equal(undefined);
                expect(result.new.a).to.equal("a");
                expect(result.new.n).to.equal(123456);
                expect(result.new.t).to.equal(time_now);
                
                //read back the document(s)
                const docs = await db.query(aql `FOR document IN ${coll} RETURN document`);
                var count = 0;
                console.log("documents read:");
                for await (const doc of docs) {
                    count++;
                    console.log(doc.t);
                    expect(doc.t).to.equal(time_now);
                }
                expect(count).to.equal(1);
                console.log("done.");

                done();
            } catch (err) {
                console.error("ERROR: " + err.message);
            }
        };

        main();
    });

});
