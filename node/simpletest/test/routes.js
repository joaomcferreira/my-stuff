var supertest = require('supertest');
var chai      = require('chai');
var app       = require('../src/index.js');
var expect    = chai.expect;
var request   = supertest(app);

describe('Task API Routes', function() {

  beforeEach(function(done) {
    done();
  });

  describe('GET /simpletest', function() {
    it('returns a list with 2 objects', function(done) {
      request.get('/simpletest')
        .expect(200)
        .end(function(err, res) {
          expect(res.body).to.have.lengthOf(2);
          done(err);
        });
    });
  });

});
