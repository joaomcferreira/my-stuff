var express = require('express');

var app = express();

app.get('/simpletest', function(req, res) {
    return res.json([
        { name:   'Jane', city: 'London', t: new Date() },
        { name: 'Sigurd', city:   'Oslo', t: new Date() }
    ]);
});

app.listen(3000, function() {
    console.log('API up and running (port 3000)');
});

module.exports = app;
