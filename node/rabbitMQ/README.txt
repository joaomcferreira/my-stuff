=================================================
notes and tests using rabbitMQ with nodejs

=================================================
docker run --hostname my-rabbit --name some-rabbit -p 4369:4369 -p 5671:5671 -p 5672:5672 -p 15672:15672 rabbitmq
docker exec some-rabbit rabbitmq-plugins enable rabbitmq_management
docker stop some-rabbit
http://localhost:15672/ guest guest

=================================================
npm install amqplib

=================================================
https://gist.github.com/carlhoerberg/006b01ac17a0a94859ba
https://github.com/benbria/node-amqp-connection-manager/blob/master/test/ChannelWrapperTest.js
https://www.npmjs.com/package/amqp-connection-manager
https://www.cloudamqp.com/docs/http.html
