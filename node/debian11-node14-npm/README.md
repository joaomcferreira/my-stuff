# debian11-node14-npm

This project is a base Docker image for other projects requiring nodejs and friends.

All npm packages are installed in /app/node_modules. It should be possible to grow the contents of this image until it can be used by as many node projects as possible.

Any Docker project using this base image is expected to simply copy their code into the /app folder and that includes replacing the existing package.json file. If all is well no package installations shoudld be required (but can nevertheless be peformed). 

Enjoy

# ChangeLog

wip, 20201118

wip, 20201117
