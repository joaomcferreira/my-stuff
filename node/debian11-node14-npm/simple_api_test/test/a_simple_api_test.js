var supertest = require('supertest');
var chai      = require('chai');
var app       = require('../src/index.js');
var expect    = chai.expect;
var request   = supertest(app);

describe('a simple API routes test', function() {
    it('basic health check for node, express and mocha', function(done) {
        request.get('/simpletest')
            .expect(200)
            .end(function(err, res) {
                expect(res.body).to.have.lengthOf(2);
                done(err);
            });
    });
});
