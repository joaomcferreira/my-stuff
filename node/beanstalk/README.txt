start beanstalk service
- docker build -t btkld .
- docker run -p 11300:11300 btkld

install beanstalk js client lib
- npm install nodestalker

publish messages
- node src/addjob.js

receive messages
- node src/getjobs.js

stop docker (stops all containers)
- docker ps -aq | xargs docker stop
