SESSION="tmux_test"

tmux new-session -d -s $SESSION
tmux send-keys 'top -d 9' C-m

tmux split-window -h -t $SESSION:0
tmux send-keys 'top -d 1' C-m

tmux split-window -v -t $SESSION:0.1
tmux send-keys 'top -d 2' C-m

tmux split-window -v -t $SESSION:0.2
tmux send-keys 'top -d 3' C-m

tmux attach

# SESSIONEXISTS=$(tmux list-sessions | grep $SESSION)
# if [ "$SESSIONEXISTS" = "" ]
# then
# fi

# https://superuser.com/questions/354844/cannot-resize-tmux-pane-from-bash-script
