package com.mycompany.app;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest
{
    /**
     * Xone Test :-)
     */
    @Test
    public void shouldAlsoWhatever()
    {
        System.out.println("====================================================");
        Runner x = new Runner();
        System.out.println("====================================================");
        x.please_do("cook a meal for lunch");
        System.out.println("====================================================");
    }

    /**
     * Xone Test :-)
     */
    @Test
    public void shouldXone()
    {
        String echo = "=";
        System.out.println("====================================================");
        Ctx ctx = new Ctx();
        ctx.sleeper.sleepBit(1);
        System.out.println("====================================================");
        echo = ctx.logger.logError("hello, how do you do ?");
        ctx.sleeper.sleepBit(1);
        System.out.println("====================================================");
        echo = ctx.logger.logInfo(echo);
        ctx.sleeper.sleepBit(1);
        System.out.println("====================================================");
        assertTrue( true );
        ctx.sleeper.sleepBit(1);
        assertEquals( echo, "_INFO ERROR hello, how do you do ?" );
    }

    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
}
