package com.mycompany.app;

class Biz {

  public int do_business(Ctx ctx, String mission) {
    ctx.logger.logInfo("doing my business...");
    ctx.logger.logInfo(mission);
    ctx.logger.logInfo("done my business.");
    return 123;
  }

}
