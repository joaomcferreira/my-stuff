package com.mycompany.app;

class Runner {

  public int please_do(String mission) {
    Ctx ctx = new Ctx();
    Biz biz = new Biz();//biz object is expected to know how to perform the mission

    ctx.logger.logInfo("hello, how do you do ?");
    ctx.logger.logInfo("I am going to " + mission);

    return biz.do_business(ctx, mission);//mission could be a JSON string...
  }

}
