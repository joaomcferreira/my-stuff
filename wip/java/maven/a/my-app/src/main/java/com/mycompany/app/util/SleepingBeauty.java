package com.mycompany.app;

class SleepingBeauty implements ISleeper {

  public int takeANap(int seconds) {
    try {
      Thread.sleep(1000 * seconds);
    }
    catch (Exception e) {
    };

    return seconds;
  }

  public int sleepBit(int seconds) {
    return takeANap(seconds);
  }

}
