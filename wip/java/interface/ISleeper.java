interface ISleeper {
  public int takeANap(int seconds);
  public int sleepBit(int seconds);
}
