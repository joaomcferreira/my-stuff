class Ctx {

  ILogger  logger;
  ISleeper sleeper;

  Ctx() {
    logger  = new ConsoleLogger();
    sleeper = new SleepingBeauty();
  }

}
