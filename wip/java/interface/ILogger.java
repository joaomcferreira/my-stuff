interface ILogger {
  public void   logFatal(String s);
  public String logError(String s);
  public String logInfo (String s);
}
