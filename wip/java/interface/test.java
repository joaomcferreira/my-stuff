class Main {
  public static void main(String[] args) {
    Ctx ctx = new Ctx();
    ctx.logger.logInfo("hello, how do you do ?");
    ctx.sleeper.sleepBit(1);
    ctx.logger.logError("3");
    ctx.sleeper.sleepBit(1);
    ctx.logger.logError("2");
    ctx.sleeper.sleepBit(1);
    ctx.logger.logError("1");
    ctx.sleeper.sleepBit(1);
    ctx.logger.logError("0");
    ctx.sleeper.sleepBit(1);
    ctx.logger.logInfo("bye, bye, have a nice day");
  }
}
