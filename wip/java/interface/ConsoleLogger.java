class ConsoleLogger implements ILogger {

  private String printItAndReturnIt(String s)
  {
    System.out.println(s);
    return s;
  }

  public void logFatal(String x)
  {
    printItAndReturnIt("FATAL " + x);
    System.exit(1);
  }

  public String logError(String x)
  {
    return printItAndReturnIt("ERROR " + x);
  }

  public String logInfo(String x)
  {
    return printItAndReturnIt("_INFO " + x);
  }

}
