import java.util.Scanner;
import java.io.File;

public class HelloWorld {
  public static void main(String[] args) {
    System.out.println("Hello, World");

    String path = "/etc/os-release";

    System.out.println("====================================");
    System.out.println("=== contents of "  +  path  + " ====");
    System.out.println("====================================");

    try {
      Scanner input = new Scanner(new File(path));
      while (input.hasNextLine())
      {
        System.out.println(input.nextLine());
      }
    }
    catch (Exception e) {
      System.out.println("Something went wrong....");
      System.out.println(e);
    }

    System.out.println("====================================");
  }
}
