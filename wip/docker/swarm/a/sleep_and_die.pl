#!/usr/bin/perl
use strict;
use warnings;

my $NAP = 2;
my $path = $ENV{PATH};

print STDERR "3 (PATH: $path)\n";
sleep $NAP;

print STDERR "2 (PATH: $path)\n";
sleep $NAP;

print STDERR "1 (PATH: $path)\n";
sleep $NAP;

print STDERR "0\n";
sleep $NAP;

print STDERR "goodbye, have a nice day\n";
sleep 1;
