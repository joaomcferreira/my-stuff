using System;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using static System.Console;

public static class Program
{
    public static void Main(string[] args) 
    {
          WriteLine("Hello from .NET Core!");
          Console.WriteLine("==============================");
          WriteLine("Environment:");
          WriteLine(RuntimeInformation.OSDescription);
          WriteLine(RuntimeInformation.RuntimeIdentifier);
          WriteLine(RuntimeInformation.FrameworkDescription);
          Console.WriteLine("==============================");

          string path = @"/etc/os-release";
          string readText = File.ReadAllText(path);
          Console.WriteLine();
          Console.WriteLine("contents of " + path + ":");
          Console.WriteLine("==============================");
          Console.Write(readText);
          Console.WriteLine("==============================");

          path = @"/etc/debian_version";
          readText = File.ReadAllText(path);
          Console.WriteLine();
          Console.WriteLine("contents of " + path + ":");
          Console.WriteLine("==============================");
          Console.Write(readText);
          Console.WriteLine("==============================");
    }
}
