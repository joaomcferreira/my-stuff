#used for Videobite tests
docker run -e MYSQL_ALLOW_EMPTY_PASSWORD=yes --mount source=vol_mysql_1,target=/var/lib/mysql -p 3306:3306 mysql:5.6

#used for OTRS test
docker run -e MYSQL_ALLOW_EMPTY_PASSWORD=yes --mount source=vol_mysql_001,target=/var/lib/mysql -p 3306:3306 mysql:5 --innodb-log-file-size=256M --character-set-server=utf8 --max-allowed-packet=64M
