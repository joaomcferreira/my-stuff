import os
import json

print('==================== registry images =================')
imgs = json.load(open('rh_images.json'))
print(imgs)

print('==================== docker login ====================')
login_cmd = 'bash docker_registry_login.sh'
print(login_cmd)
os.system(login_cmd)

from_reg = 'FROM registry.redhat.io/'
base_tag = 'rhel-t'

for img in imgs:
  if not 'tag' in img.keys():
    continue
  print('==================== rh image ========================')
  tag = base_tag + img['tag']
  from_line =  from_reg + img['base']
  print(from_line + ' (tag: ' + tag + ')')
  dockerfile = '/tmp/rh_Dockerfile_tests_' + tag
  os.system('echo "' + from_line + '" > ' + dockerfile)
  os.system('cat Dockerfile.skeleton >> ' + dockerfile)
  print('==================== docker build ====================')
  os.system('docker build -t ' +  tag + ' -f ' +  dockerfile + ' .')

for img in imgs:
  if not 'tag' in img.keys():
    continue
  print('==================== docker run ======================')
  tag = base_tag + img['tag']
  print(tag + ' ' + img['base'])
  os.system('docker run ' +  tag)

print('==================== docker images ===================')
os.system('docker images')

print('==================== done ============================')
