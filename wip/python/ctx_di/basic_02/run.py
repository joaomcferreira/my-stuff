from lib.xctx import Xctx

ctx = Xctx()

two_minus_one = ctx.functions.math_sub(2, 1)
ctx.banners.pretty(two_minus_one)
