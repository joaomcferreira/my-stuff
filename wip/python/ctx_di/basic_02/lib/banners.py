class Banners:

  def raw(self, x):
    print('-')
    print(x)

  def simple(self, x):
    print('----------')
    print(x)
    print('----------')

  def pretty(self, x):
    print('=== !! -- = -- !! ===')
    print('=== !! -- = -- !! ===')
    print(x)
    print('=== !! -- = -- !! ===')
