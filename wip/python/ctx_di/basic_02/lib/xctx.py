from .maths import Functions
from .banners import Banners

class Xctx:
  def __init__(self):
    self.functions = Functions()
    self.banners   = Banners()
