from lib.maths import Functions
from lib.banners import Banners

m_utils = Functions()
b_utils = Banners()

ctx = {
  'functions': m_utils,
  'banners':   b_utils
}

two_minus_one = ctx['functions'].math_sub(2, 1)
ctx['banners'].pretty(two_minus_one)
