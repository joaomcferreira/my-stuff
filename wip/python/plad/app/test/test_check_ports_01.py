import socket
import time

def check_connect_tcp(host, port):
  print('check tcp connect ' + host + ' ' + str(port))
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((host, port))
  time.sleep(0.1)

def test_check_ports_01():
  print('')
  for p in [ 8888, 11300, 5672, 15672 ]:
    check_connect_tcp('localhost', p)
