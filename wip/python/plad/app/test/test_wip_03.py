import requests
import time
import uuid

def get_and_check_status_200(u):
  print(u)
  res = requests.get('http://' + u)
  c = res.status_code
  print(c)
  assert 200 == c
  print(res.text)
  print('')
  return res.text

def test_wip_01():
  print('')

  u = 'localhost:8888/NameTest?'
  x = uuid.uuid4().hex

  r = get_and_check_status_200(u + 'name=' + x)
  assert r == 'will set name to ' + x

  c = 0
  name_is = 'name is ' + x
  for i in range(10):
    if c > 2:
      break
    print(i)
    time.sleep(1)
    r = get_and_check_status_200(u + 'name')
    if r == name_is:
      c = c + 1
  assert r == name_is
  assert c > 2
