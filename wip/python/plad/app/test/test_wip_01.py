import requests

def check_generic_http(u):
  print(u)
  r = requests.get('http://' + u)
  j = r.json
  print(j)
  c = r.status_code
  print(c)
  assert 200 == c
  assert 'x_200' == 'x_' + str(c)

def check_scenario_b():
  check_generic_http('www.debian.org/download')

def test_wip_01():
  print('')
  check_scenario_b()
