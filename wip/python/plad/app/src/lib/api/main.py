import cherrypy

class Api:

  def set_ctx(self, ctx):
    self.name = 'NoName'
    self.ctx = ctx

  @cherrypy.expose
  def index(self):
    return '''<form action="greetUser" method="GET">
What is your name?
<input type="text" name="name" /><input type="submit" />
</form>
'''

  @cherrypy.expose
  def greetUser(self, name=None):
    if name:
      return self.ctx.name + " Hey %s, what's up?" % name
    else:
      if name is None:
        return 'Please enter your name <a href="./">here</a>.'
      else:
        return 'No, really, enter your name <a href="./">here</a>.'

  @cherrypy.expose
  def NameTest(self, name=None):
    if name:
      self.name = name
      return "will set name to %s" % name
    else:
      return "name is %s" % self.name

  @cherrypy.expose
  def txmsg(self, msg='TestMessage01'):
    print('======================================')
    host = 'localhost'
    queue = 'hello'
    routing_key = queue
    body = msg
    self.ctx.msg.tx_1_msg(host, queue, routing_key, body)
    print('======================================')

  @cherrypy.expose
  def rxmsg(self):
    print('======================================')
    host = 'localhost'
    queue = 'hello'
    routing_key = queue
    msg = 'WIP' # self.ctx.msg.rx_1_msg(host, queue, routing_key)
    print('======================================')
    return "msg is %s" % msg
