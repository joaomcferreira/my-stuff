from .cfg import Cfg
from .msg import Msg

class Ctx:
  def __init__(self):
    self.cfg  = Cfg()
    self.msg  = Msg()
    self.name = 'I am Plad Ctx! Good day to you!'
