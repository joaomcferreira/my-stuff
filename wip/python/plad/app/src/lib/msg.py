import pika

class Msg:
  def tx_1_msg(self, host, queue, rk, body):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=host))
    channel = connection.channel()
    channel.queue_declare(queue=queue)
    channel.basic_publish(exchange='', routing_key=rk, body=body)
    print(" [x] Sent '" + body + "'")
    connection.close()
