import os.path

class Cfg:
  def get(self, key):

    if key == 'http_server_config_path':
      x = os.path.join(os.path.dirname(__file__), '..', '..', 'cfg','web.conf')
      return x

    raise Exception('configuration key ' + key +  ' not supported')
