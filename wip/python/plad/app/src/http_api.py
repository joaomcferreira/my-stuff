import os
os.system('python3 --version')

import cherrypy

from lib.api.main import Api
from lib.ctx      import Ctx


if __name__ == '__main__':

    ctx = Ctx()
    api = Api()
    api.set_ctx(ctx)
    print(api.ctx.name)

    cfg_path = ctx.cfg.get('http_server_config_path')
    print(cfg_path)

    cherrypy.quickstart(api, config=cfg_path)
