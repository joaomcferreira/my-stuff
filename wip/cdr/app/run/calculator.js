const { CoolMath } = require('../lib/cool_math.js');

const x = 6;
const y = 3;
const z = 0;

console.log(`${x} + ${y} = ${CoolMath.sumTwoValues(x, y)}`);
console.log(`${x} + ${z} = ${CoolMath.sumTwoValues(x, z)}`);
