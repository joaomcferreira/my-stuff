var chai = require('chai');
var xpct = chai.expect;

const { CoolMath } = require('../lib/cool_math.js');

describe('CoolMath', function() {
  describe('Basic Arithmetic operations', function() {

    it('should be able to sum two positive integers', function(done) {
        xpct(CoolMath.sumTwoValues(100, 1)).to.equal(101);
        done();
    });

    it('should respect the neutral element property of addition', function(done) {
        xpct(CoolMath.sumTwoValues(101, 0)).to.equal(101);
        done();
    });

  });
});
